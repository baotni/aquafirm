 //////////////////////////////////////////////////////////////////////////////////////////
///    1. New outlook design and hardware version 2.0
///    2.  New PCB board using A4950 driver
///    3. SERIES number change to KH0-00-00059 
///    4. timer[21] changed to timer=22307 means 2 hours/measure, sleep time 23:00 to 07:00
///    5. Air pump: 12V air pump, add sleep mode. 
///    6. add Tank KH verification/repeat function if KH error or distance > 0.1 KHspc fixed = 0.1
///    7. Reduce PH stable time to 900 sec (15 min) to shorten total measure time
///    8. If KH calibration (trueKHflag = true) then manually start over again
///    9. Output support aquawiz standard data trnsmittion code: 
///       1st pulse: interrupt, 2nd pulse: 1 sec = 0.1 dKH, 3rd pulse: negative value
///    10. Watch dog function to reset CPU if crash like pump runaway
///    11. Serial number assigned after 1st power up
///    12. no test mode and thingspeak
///    13. water level switch to control reference water, add Fill, Refill item
///    14. delete "off dos CaRx" selection
///    15. access web time from server.aquawiz.net using API /currentdate
///    15. correction factor TankKH * 1.3  to fit salifert KH result
///    16. 魚缸蠕動馬達向右轉，正反交換
///    17. test mode: 當 target KH=1.23 時，啟用 testmode, 30 min測一次，取消repeat, 
///    20. display LF = 100 if LF > 100
///    21. quit AP after connection closed. AT+CWQAP
///////////////////////////////////////////////////////////////////////////////////////////   
#include <EEPROM.h>  // save parameter to EEPROM
#include <Wire.h>  // 1602 LCD I2C
#include <LiquidCrystal_I2C.h> //1602 LCD I2C
#include <TimeLib.h>
#include <Adafruit_MCP4725.h> // DAC for KH2PH output
#include <avr/wdt.h>  // Watch dog timer to reset CP
//LiquidCrystal_I2C lcd(0x3F, 16, 2);
LiquidCrystal_I2C lcd(0x27, 16, 2);
LiquidCrystal_I2C lcd1[2]={LiquidCrystal_I2C(0x27, 16, 2),
                           LiquidCrystal_I2C(0x3F, 16, 2)};
                          
//byte resetEPROM = 0  ;  // 0: not reset; 1: reset all;  2:reset some                                                                           ;
int testmode = 0; // 0: no test mode, 1: test mode, delete repeat
char SER[13] = "KH1-00-00000";
int setSER=1;
int lcdadr = 0;
char SSID[33] = "Tayler_Home"; 
char PASS[33] = "0922473267";   
char WebKey[6] = "CnBA";
int timezone = 8;
int wifichannel = 5;
//int time_adj = 200;   // adjust time in sec/hour time adjust factor
bool online = true;

///// timer and air pump sleep on off /////
int Timer=10000 ;
int Timer1=1 ;
int Air_off=0 ;
int Air_on=0 ;
bool sleep=false;

//////// web parsing ///////
String url[4] ={"","","",""} ;   //url[0] - url[3]
String MSG ="";

///////// wifi flag ////////
bool newssid = false;   // change AP ssid and password flag
bool c_ap = false;    // connection to AP ok or not
bool c_svr;           // connection to server ok or not
bool yesno;          // server reply ok or not for GET and POST


  //////// I/O assignment  //////
//const byte relay[6] = {11,10,3,5,6,9};  // 11,10,3,5 ,6,9
const byte relay[6] = {11,10,5,3,6,9};  // 魚缸蠕動馬達向右轉，正反交換
const byte input[3] = {8,7,4};
/*
#define relay0  3 // pump 1     tank
#define relay1  5 // pump 1     
#define relay2  6 // pump 2     ref 
#define relay3  9 // pump 2      
#define relay4  10 // pump 3     change 
#define relay5  11 // pump 3  
  // 8, INPUT);    // up button as input
  // 7, INPUT);    // down button as input
  // 4, INPUT);    // select button as input
*/
#define Dospump 12 // KH dosing pump
#define Airpump  13 // air pump on off I/O 
#define KHO  2 // KH output jacket on/off
#define waterlevel  16 
#define onPWM  255

  
// Structure Object for EEPROM
const int eeAddress = 0; // EEPROM start address
struct KHparameter {
  int setSER;
  int testmode;
  int mldKH;
  int maxml;
  int hundccsec;
  int RefKH;
  int SetTankKH;
  int Pump1on;
  int Pump2on;
  int Timer;
  int timezone; 
  int wifichannel; 
  int lcdadr;
  char WebKey[6];
  char SSID[33];
  char PASS[33];
  char SER[13];
};

// Process step parameter
byte StepId= 0;
bool Halt = false;

//////// Control timer parameter //////
int Pump1on=27, Pump2on=38; // Pump1on=22, Pump2on=33; Pump1rev, Pump2rev;  // Pump on and reverse time
int StablePH=900; // StablePH 900  PH equilibrium stable time 
int RefPHtime=60;  // RefPHtime  60 sec, PH probe response time from PH8.5 to 9.5 in PH calibration

/////// PH related parameter /////
#define Range 5318;       // PH A/D range simulator PH10-PH7 
int PH7=10, PH10=Range;          // PH calibration parameter
long total=0, PHvalue=0, TankPH=0, RefPH=0;   // PH parameter 
int SetTankKH=7500, TankKH=-1, RefKH=7500, tempKH, trueKH; // TankKH=-1 => program just started, not display on lcd
bool NewKHflag = false, trueKHflag = false, KHrep=false; // program start, not measured => NewKHflag=false, not repeat measure KHrep = false; 
                                                         // not input true KH => trueKHflag =false
/////// dosing related parameter //////
int mldKH=220, maxml=0, max01sec=600, hundccsec=142, KHdos=0;

/////// water level related ///////
bool Hmark= true, Lmark=true;

// Lcd keypad Button setup
byte Menu = 1; 
bool Action = false;
 
////// DAC setup //////
Adafruit_MCP4725 dac;  // far away from LCD setup

////////////////////////////////////
void setup() {

wdt_disable();  // disable watch dog function  
byte i; 
    
    // LCD setup
    lcd1[0].begin();lcd1[1].begin();lcd.begin();  
    // wifi i/o setup  
    Serial.begin(115200); // ESP8266 baur rate 115200 
    Serial.println(F("AT+CWQAP"));  // disable auto connect to AP
// structure variable P 
  //EPROMsave();    // Reset EPROM for new PCB board or parameter structure
  KHparameter P;                 //Variable P to store custom object KHparameter 
  EEPROM.get(eeAddress, P);   //read from EEPROM.
    setSER = P.setSER;
    testmode = P.testmode;
    mldKH=P.mldKH;//
    maxml=P.maxml;//
    hundccsec=P.hundccsec;
    RefKH = P.RefKH;
    SetTankKH= P.SetTankKH;//
    Pump1on = P.Pump1on;//
    Pump2on = P.Pump2on;//
    Timer = P.Timer;  //
    timezone = P.timezone; //
    wifichannel = P.wifichannel;
    lcdadr = P.lcdadr;
    strcpy (WebKey, P.WebKey);
    strcpy (SSID, P.SSID);//
    strcpy (PASS, P.PASS);//
    strcpy (SER,  P.SER);

    if (SetTankKH == 1230) {testmode=1;} else {testmode=0;}   // set testmode from web if targetKH = 1.23
//////// LCD display ////
    lcd = lcd1[lcdadr]; // set LCD address 
    lcd.clear(); lcd.print(F("  KH controller "));
    lcd.setCursor(0,1); lcdspace2();lcd.print(SER); 
     
    //if (resetEPROM !=0) {lcd.setCursor(0,1);lcd.print(F("Reset OK        ")); delay(2000);}
    Timerset();    // setup Timer1, AIr_off, Air_on time
    // DAC begin
    dac.begin(0x60);  
       
    // button setup
    for (i=0; i<=2; i++) {  pinMode(input[i], INPUT);}
         
 if (setSER==1) {   // 
     // LCD select i2c address ///
     lcd.clear();lcd.print(F("LCD 0"));
     lcdadr = Adj(lcdadr,1, 0, 1, 0, 7);
     
 } // if set SER
     
    // Relay digital output setup  
    for (i=0; i<=5; i++) { pinMode(relay[i], OUTPUT); digitalWrite(relay[i], LOW);  } 
    pinMode(KHO, OUTPUT); digitalWrite(KHO, HIGH); //光電隔離，high->low low-> high
    pinMode(Dospump, OUTPUT); digitalWrite(Dospump, LOW); 
    pinMode(Airpump, OUTPUT); digitalWrite(Airpump, LOW);
    
    // check air pump on-off
    digitalWrite(Airpump, HIGH); delay(3000); digitalWrite(Airpump, LOW);  delay(200); 
    
    //Initiate and Check pump 4950 driver
    for (i=0; i<=4; i+=2) {
      digitalWrite(relay[i], HIGH); digitalWrite(relay[i+1], LOW); delay(3000);
      digitalWrite(relay[i], LOW); digitalWrite(relay[i+1], LOW); delay(200);
      digitalWrite(relay[i], LOW); digitalWrite(relay[i+1], HIGH); delay(3000);
      digitalWrite(relay[i], LOW); digitalWrite(relay[i+1], LOW); delay(200);  } 
      
  if (setSER==1) {    
      // check KH dosing pump
      digitalWrite(Dospump, HIGH); delay(3000); 
      digitalWrite(Dospump, LOW); delay(200);
    
      // check KH control output relay
      digitalWrite(KHO, LOW); delay(2000); digitalWrite(KHO, HIGH); delay(200);  
    
      // DAC check
      dac.setVoltage(dacKH(10000), false); // set dac = ph 10
     
      // check PH
      while (ReadButton()==0) {
      ReadPHvalue(); DisplayPHvalue();
        lcd.setCursor(9,1);if(digitalRead(waterlevel)){lcd.print (F("H"));} else {lcd.print (F("L"));}
      }  // while
     
      // ESP 8266 wifi data transfer setup
      SyncWeb(); lcd.clear();DisplayPHvalue();delay(2000); // to display clock
  
      //// Set serial number /////
      AdjSER(); 
    
    } // if setSER  
    dac.setVoltage(dacKH(7000), false);  // reset dac  
    
}
///////////////////////////
/////////Menu Loop//////////////
///////////////////////////
void loop() {
    unsigned long C_start= millis();   // start count 
    static unsigned long Elapse; int Count=Elapse/200;
    int menutime=100;
      switch(ReadButton()) { 
      case 3:  Menu ++; if (Menu > 4) {Menu = 1;} break;    //if down (increase)
      case 2:  Menu --; if (Menu < 1) {Menu = 4;} break;    //if up   (decrease)  
      case 5:  Action = true; break;    //if Enter (Action) 
      }
              
      // if not hit button for 30 sec, then auto run KH control      
     if (Count > menutime) {Menu = 5; Action = true;}    // idle 0.2 *100 = 20 sec 

     if (Count%3 != 0) {       // flash 0.4 sec on
         //////////// lcd print top menu  
            lcd.setCursor(0,0); lcd.print (F("  Wifi   Sync   "));
            lcd.setCursor(0,1); lcd.print (F("  Pump   Other  "));
     }  // if Count%3
     
   switch (Menu) {
    case 1:
        lcd.setCursor(0,0);
        if (Action) {WebServer(); }    
        break;
    case 2:
        lcd.setCursor(8,0); 
        if (Action) {
                     //lcd.clear(); lcd.setCursor(0,0); lcd.print(F("Sync    ")); 
                     //lcd.setCursor(0,1); lcd.print(F("Web Data & Input"));  
                     SyncWeb();
                     }  
        break;
    case 3:
        lcd.setCursor(0,1);
        if (Action) {
          //if (StepId == 0 || StepId == 5 || StepId == 11) {
            AdjLevel();
          //  }// if back to KH control, start from beginning
        }
        break;
    case 4:
        lcd.setCursor(8,1);
        if (Action) {KH2PH();} 
        break;       
    case 5:
        if (Action) {ControlKH();} 
        Menu = 1;    // reset Menu to 1
        break;   
     } // switch
       if (Count%3 == 0) {lcd.print (F("        "));} // flash 0.2 sec off
       Elapse += (unsigned long)(millis() - C_start); 
       Count = Elapse/200;     // 0.2 sec timer
       if (Action) {Elapse = 0;}   // back to menu start count time 
       Action = false;
  
}  // loop
////////////////////////////
////////////////////////////
void Timerset() {
  Timer1=Timer/10000;
  Air_off=Timer/100 - Timer1*100;
  Air_on =Timer - Timer1*10000 - Air_off*100; 
}


/////////////// save KH parameter to EEPROM ////////
void EPROMsave() {

  KHparameter P;         // structure variable P to EEPROM
    P.setSER = setSER;
    P.testmode = testmode;
    P.mldKH=mldKH;
    P.maxml=maxml;
    P.hundccsec=hundccsec;
    P.RefKH = RefKH;
    P.SetTankKH = SetTankKH;
    P.Pump1on = Pump1on;
    P.Pump2on = Pump2on;
    P.Timer = Timer;  
    P.timezone = timezone;
    P.wifichannel = wifichannel;
    P.lcdadr = lcdadr;
    strcpy (P.WebKey, WebKey); 
    strcpy (P.SSID, SSID);  
    strcpy (P.PASS, PASS); 
    strcpy (P.SER, SER);
   
   EEPROM.put( eeAddress, P );   
}

//////////////////////////////
int ReadButton() {           // up, down, and select only return once
  
  byte State = 0; static byte lastbutton;        // initial State = 0
  
  if (digitalRead(input[0]) == 0) { State = 2;}  // read down button
  if (digitalRead(input[1]) == 0) { State = 3;}  // read up button
  if (digitalRead(input[2]) == 0) { State = 5;}  // read select button
   
  if (State == 0 || State > lastbutton) {     
     lastbutton = State; return State;}
  else { return 0;}
}

/////////// ESP8266 Web Server start to input SSID PASS //////////

////////////////////////////
void WebServer () {
  char ipd[] = "IPD,";  char ok[] = "OK";
  if (APServer()) {               // Setup ESp8266 as AP mode and Server mode
               lcd.clear(); lcd.print(F("WiFi Aquawiz_KH "));
       lcd.setCursor(0, 1); lcd.print(F("Web 192.168.4.1 "));}
  else { lcd.setCursor(0, 1);lcd.print(F("   WiFi Fail !  ")); delay(2000); return;}                 
  ////////// wait for client login ///////////////////////             
  ESPreply(ipd, 300); // wait 5 min for client to login in
   ///////// serve homepage ////////
     while(1) { 
       lcd.clear(); lcd.print(F("Wifi Setup Page "));
       serve_homepage();// send webpage 2 times
       ///////// wait for message come in /////////////////
       lcd.setCursor(0, 1); lcd.print(F("Key-in & Submit "));
       if (clientmsg()) {       // if client has key-in ssid, password
                 lcd.clear(); lcd.print(F("Connect WiFi    ")); 
                 Serial.println(F("AT+CWMODE=1")); ESPreply(ok, 2);  connectWiFi();    // escape AP mode to sta mode and force phone to find other ssid
                 lcd.clear();lcd.print(F("SSID :"));lcd.setCursor(0,1); lcd.print(SSID); delay(2000); 
                 lcd.clear();lcd.print(F("Password :"));lcd.setCursor(0,1);lcd.print(PASS); delay(2000); 
                 lcd.clear();lcd.print(F("Time Zone :"));lcd.setCursor(0,1);lcd.print(timezone); delay(2000); 
                 EPROMsave();break;    // save SSID and PASS to EPROM  
       }  // if clientmsg
   }  //while

}  //void

/////////// send home page to input ssid and pass ////////
void serve_homepage() {
   char prompt[] = ">"; int  urllength = 1091; byte i;  // client channel ID  //1091
   //////////////////////
   delay(1000);  // wait for broser ready for homepage
   ////// content /////   
   for (i = 0; i <2; i++) {    // send 2 times home page  
    //////// CIPSEND command send url length   //////////
     delay(1000);  // wait for broser ready for homepage
     Serial.print(F("AT+CIPSEND=0,")); Serial.println(urllength); 
    //////// sent url to client ////////
     ESPreply(prompt, 2); if (yesno) {
        Serial.print(F("HTTP/1.1 200 OK\r\ncontent-type: text/html; charset=utf-8\r\ncontent-length: "));  //77
        Serial.println(F("984\r\nConnection: close\r\n"));  //29 //984
        Serial.println(F("<html style=\"background-color:powderblue;\"><head><script>ssid=\"\";pawd=\"\";time=\"\";function ST(f1) {var request = new XMLHttpRequest();ssid=\"ssid=\"+f1.ssid.value+\"&\";pawd=\"password=\"+f1.password.value+\"&\";time=\"time=\"+f1.time.value+\" \";request.open(\"GET\",\"ajax_inputs\"+ssid+pawd+time,true);request.send(null);alert(\"WiFi Settings saved in KH Controller\");window.location.assign(\"http://www.aquawiz.net\");return false;}</script><span style=\"font-family: arial,helvetica,sans-serif\"><h1 style=\"font-size:300%;\">KH Controller Wifi Setup</h1><form style=\"font-size:200%;\" onsubmit=\"return ST(this)\">WiFi:<br><input type=\"text\" id=\"ssid\" name=\"ssid\" style=\"font-size:100%;\"><br><br>密碼 Password:<br><input type=\"password\" id=\"password\" name=\"password\" style=\"font-size:100%;\"><br><br>時區 Time Zone: GMT +<br><input type=\"text\" id=\"time\" name=\"time\" style=\"font-size:100%;\"><br><br><input type = \"submit\" class=\"button4\" value=\"連線 Connect\" style=\"font-size:100%;\"></form></span></html>"));
        Serial.println(); Serial.println();
        }   // if prompt
      delay(100);
    } // for i 
}    
///////////////////////////////////////////////////////
bool clientmsg ()  {
    char ssid[] = "ssid="; char pass[] = "password=" ; char tim[] = "time=" ; //char api[] = "api=" ;
    char msg[49]; msg[0]='\0' ; int last_pos =0; float fl; //char aquawiz[]= "aquawiz"; 
   /////// get message ////////
  cleanbuffer();  // clean buffer before get new message
   //Serial.setTimeout(1000);
   while (!Serial.available()); {} // wait.....
  
    ////////////////// no settankKH, trueKH, Timer1 version //////////////
      if (Serial.find(ssid)) {last_pos = Serial.readBytesUntil('&', msg, 48); msg[last_pos] = '\0'; } else {return false;} 
           urldecode(SSID, msg); newssid = true;     
      Serial.find(pass); last_pos = Serial.readBytesUntil('&', msg, 48); msg[last_pos] ='\0'; 
           urldecode(PASS, msg); 
      Serial.find(tim); fl = Serial.parseInt(); 
           timezone = fl;    
    
      return true;
}
////////// Convert url %20 %23 ... to ASCII ////////// 
void urldecode(char *dst, const char *src) {
 char a, b;
  while (*src) {
    if ((*src == '+')) { *dst++ = ' '; src++;}
    if ((*src == '%') && ((a = src[1]) && (b = src[2])) && (isxdigit(a) && isxdigit(b))) {
       if (a >= 'a')  a -= 'a'-'A'; if (a >= 'A')  a -= ('A' - 10);  else  a -= '0';
       if (b >= 'a')  b -= 'a'-'A'; if (b >= 'A')  b -= ('A' - 10);  else  b -= '0';
       *dst++ = 16*a+b;  src+=3;  }  // if src == % 
    else { *dst++ = *src++; }
  }  // while
  *dst++ = '\0';
 
}
//////////clean serial buffer to accept new come in message ////////////////
 void cleanbuffer() {
  unsigned long Countstart= millis(); long timeout = 1000;
      while (millis()-Countstart < timeout) {
          if (Serial.available()) { Serial.read(); Countstart = millis(); } // if serial not available > timeout then end
      }  // while
 }

/////////// check ESP reply OK, IPD, /////////////////// 

void ESPreply(const char* com, int timeout)  {
   
   /////// get message ////////
    char buf[10] = {'\0'}; strcpy(buf, (char *) com);
    long TO = timeout*1000; 
    Serial.setTimeout(TO);   
         if (Serial.find(buf)) {yesno= true; return; } else {yesno= false; }
  
} 

///////////// check ESP client connect to server status, 3 = connected ///////
void ESPstatus()  {
   /////// get message ////////
    char colon[] = ":";    // STATUS:3
     char msg[5]; msg[0] = '1'; msg[1]='\0' ; 
     
     Serial.println(F("AT+CIPSTATUS"));
    Serial.setTimeout(5000); 
    if (Serial.find(colon)) {Serial.readBytes(msg, 1);  msg[1] = '\0';} else {yesno = false; c_svr = false; }
    switch (msg[0]) { 
      case '3': c_ap = true;  c_svr = true;  yesno = true;  break; //status: 3 connected 
      case '1': c_ap = false;  break; // no msg, =1
      case '2': c_ap = true;   break; // status: 2 got IP
      case '5': c_ap = false;  break; // status: 5 not connect to AP
      case '4': c_ap = true;   break; // status: 4 connect to AP not connect to SVR
      } 
      //lcd.setCursor(15,0); 
      if (msg[0] != '3') {c_svr = false; yesno = false; //lcd.print(msg); delay(1000);
      } 
      //else {lcd.print(F(" "));}
} 

/*
///////// Find WiFi AP SSID ////////// 
void findSTA()  {
    char msg[33]; msg[0]='\0' ; int last_pos = 0; 
    Serial.setTimeout(2000);
    Serial.find('"'); //  "Tayler_Home" 
        last_pos = Serial.readBytesUntil('"', msg, 32);  msg[last_pos] = '\0';
        if (last_pos ==0) {c_ap = false;} else {c_ap = true;}
        if (strcmp(msg, SSID)==0) {newssid = false;} else {newssid = true;} 
       
}   
*/
//////////// get Server reply message /////////
void SVRreply ()  {
  
    char datastart[] = "((";
    char msg[35]; msg[0]='\0' ;  bool extramsg;
    int last_pos;
   /////// get message ////////
   Serial.setTimeout(5000);
   if (!Serial.find(datastart)) {c_svr = false; return;}
   last_pos = Serial.readBytesUntil(')', msg, 10);  msg[last_pos] = '\0';
   if (last_pos == 0 || last_pos == 10) {c_svr = false; return;}
   c_svr = true; c_ap = true; MSG = String(msg); 
   
   /////////// judge reply is yes or no///////// 
   if (MSG =="no" || MSG == "invalid") {yesno= false; } else {yesno = true;} 
   
   /////// get extra message ////////////
   if (MSG == "invalid") {       // there will have error message
       if (!Serial.find(datastart)) extramsg = false; else extramsg = true; 
       if (extramsg) {last_pos = Serial.readBytesUntil(')', msg, 32);  msg[last_pos] = '\0';}
       if (last_pos == 0 || last_pos == 32) extramsg = false; extramsg = true;
       if (extramsg) {MSG = String(msg); lcd.setCursor(0,1); lcd.print(MSG); lcdspace2();delay(5000);}
       } // if MSG
}

//////////// get Server reply message and Date: /////////
void SVRtime()  {
    char date[] = "Date: "; char space[] = " "; char colon[] = ":"; 
    char msg[30]; msg[0]='\0'; //char webwday[4]; 
    int last_pos; int webhour, webmin, websec; //webday, webweekday; 
    static time_t old_t; long t_kh_t, t_old_t;
    Serial.setTimeout(5000);
   //////// get server time /////////Date: Sat, 01 Apr 2017 11:34:53 GMT
    if (!Serial.find(date)) {c_svr = false; //lcd.setCursor(15,0); lcd.print("S"); 
                     return;}  c_svr = true; c_ap = true;
    
    Serial.readBytes(msg, 3);  msg[3] = '\0';  //Date: Sat,   get webweekday//////
      //switch (msg[1]) { case 'u': if (msg[0] == 'S') webweekday=0; else webweekday =2; break; case 'o': webweekday=1; break; case 'e': webweekday=3;break; case 'h': webweekday=4; break; case 'r': webweekday=5; break; case 'a': webweekday=6; break;} 
    Serial.find(space); Serial.find(space); Serial.find(space);   //Date: Sat, 01 Apr 2017 
    Serial.find(space); Serial.readBytes(msg, 2);  msg[2] = '\0'; webhour = atoi(msg);    ////// get userhour
    Serial.find(colon); Serial.readBytes(msg, 2);  msg[2] = '\0'; webmin = atoi(msg);   ////// get webmin
    Serial.find(colon); Serial.readBytes(msg, 2);  msg[2] = '\0'; websec = atoi(msg);   ////// get web second
   
   /////////// calculate user time and set arduino time     
   //webday = webweekday +1; 
   //time_t kh_t = now();  // remember current kh system time for time adjustment
   setTime(webhour, webmin, websec, 1, 1, 2017);  // 2017/1/1, sunday -> weekday =0
   time_t t = now(); t += timezone*3600; setTime(t);   // set time with time zone
   /*
    if (t < kh_t) {t_kh_t = -1*(long)(kh_t-t);} else {t_kh_t = (long)(t-kh_t);} /// calculate (unsigned - unsigned) 
    t_old_t = t-old_t;
    if (abs(t_old_t) < 30000) { //lcd.setCursor(4, 1); lcd.print(t_kh_t); lcd.print(" "); delay (5000);
                              time_adj += (t_kh_t*1000/(t_old_t/36));} ////////// get calibration factor time_adj sec per 10 hours, if within 10 hour///////
   if (time_adj > 300) {time_adj = 300;}                     // limited time_adj < 300 sec/10 hour 
    if (time_adj < -300) {time_adj = -300;}                     // limited time_adj < -300 sec/10 hour 
    ////////// get calibration factor time_adj sec if within 10 hour, 
    //if > 10 hours means just power up, old_t=0, 
    //if too frequnt call WebTime, t_old_t =0, time_adj =0 ///////
   old_t = t;
   */
} 
/*
void lcdprintesp ()  {
   // char buf[6] = {'\0'}; strcpy(buf, ipd);
    char msg[600]; msg[0]='\0' ; int last_pos = 0; byte button =0;
     unsigned long Countstart= millis(); long timeout = 3000;
   
   ////// another way to get message //////
      while (millis()-Countstart < timeout) {
          if (Serial.available()) {
                  char c = Serial.read();
                  msg[last_pos]=c ;last_pos++;
                  Countstart = millis();  // if serial not available > timeout then end
          } // if
          if (last_pos > 599) {break;}
   }  // while
   msg[last_pos] = '\0';    // ending with \0    
   
   //////// print or process message ///////
   lcd.clear(); lcd.setCursor(0, 0);  lcd.print("msg size "); lcd.print(last_pos); delay (1000);
   for (int i = 0; i < last_pos;) {
      for (int R = 0; R < 2; R++) {
          lcd.setCursor(0, R); lcd.print(F("                ")); 
          for (int C = 0; C < 16; C++) {
           lcd.setCursor(C, R); 
                button = ReadButton();
                if (button >0) {i=last_pos;}
                if (i < last_pos) {lcd.print(msg[i++]) ; delay(200);}
           else {lcd.print(' ') ;}
           } //for C
      }  //for R
   } // for i
}
*/

////////////// ESP 8266 wifi data transfer setup
////// Set esp8266 as AP and Server
bool APServer() {             // mode2 to enable AP
     char ok[] = "OK";     
     String sap; int wifichannel=5;
     lcd.clear(); lcd.print(F("WiFi Setup      ")); 
     sap=F("AT+CWSAP=\"Aquawiz_KH\",\"\",");sap += wifichannel; sap += F(",0");
     ResetWiFi(); delay(2000);
     Serial.println(F("AT+CWMODE=2")); ESPreply(ok, 2); delay(1000);
         //Serial.println(F("AT+CWSAP=\"Aquawiz_KH\",\"\",wifichannel,0")); ESPreply(ok, 10); 
         //Serial.println(F("AT+CWSAP=\"Aquawiz_KH\",\"1234567890\",9,4")); ESPreply(ok, 20); // set password 1234567890, last is encoding mode 1 WEP
     Serial.println(sap);ESPreply(ok, 20); 
     Serial.println(F("AT+CIPMUX=1"));ESPreply(ok, 2);
     Serial.println(F("AT+CIPSERVER=1,80")); 
     ESPreply(ok, 2); if (yesno) {return true;} 
     return false;
}        

////////// wifi setup to connect to AP /////////
void connectWiFi() {
  char ok[] = "OK"; bool wifi;
  
     for (int retry = 0; retry < 3; retry++){
       if (retry > 0) {lcd.setCursor(0, 1);lcd.print(F("    Retry "));lcd.print(retry);lcdspace4();}
       ResetWiFi(); delay(2000);
       Serial.println(F("AT+CIPMUX=0"));ESPreply(ok, 2);
       Serial.println(F("AT+CWMODE=1")); ESPreply(ok, 2);
       if (yesno) {wifi = true;} else {wifi = false;}   // define wifi fail or not
       
       String cmd =F("AT+CWJAP=\""); cmd+=SSID; cmd+="\",\""; cmd+=PASS; cmd+="\"";
       Serial.println(cmd); 
       ESPreply(ok, 20); if (yesno) {c_ap = true; break;} else c_ap = false; // define AP fail or not
       if (!wifi)  {lcd.setCursor(0, 1);lcd.print(F("   WiFi Fail !  ")); delay(2000); }
       if (wifi && !c_ap) {lcd.setCursor(0, 1);lcd.print(F("    AP Fail !   ")); delay(1000);}  
     }  // for
    
}
    
void ResetWiFi() {
  char READY[] = "ready"; char ok[] = "OK";
  Serial.println(F("AT+RST"));ESPreply(ok, 2); 
}

//////////////////// Sync Web site //////////////
void SyncWeb() {
   byte i; byte tryi =2; char ok[] = "OK"; //char ipd[] = "IPD,";
   int Fieldvalue; //Halt = false;  // reset Halt to check timer
   String SERIES=String(SER);
 /////////////////// GET POST /////////////////////////////
    //url[1]=""; url[1] = F("Host:192.168.0.18\r\n");  // test on local
    //url[1] += IPPORT; url[1] += F("\r\n");   /// common url for GET and POST
    url[1] = F("Host: server.aquawiz.net\r\n"); 
    
    lcd.clear(); lcd.setCursor(0,0); lcd.print(F("Sync    ")); 
    lcd.setCursor(0,1); lcd.print(F("Web Data & Input"));  
    
    // Step 0 ////// get time /////////
       lcd.setCursor(4,0); lcddot();
       url[0]= F("GET /currentdate"); url[0] += F(" HTTP/1.1\r\n");   
       url[2] = F("Connection: Keep-Alive\r\n"); 
       url[3] = F("\r\n"); 
       
       for (i=0; i< tryi; i++) {UpdateIOT1(1, false); SVRtime(); if  (!c_ap || c_svr) {break;} }
       if (setSER == 1) {Serial.println(F("AT+CIPCLOSE"));ESPreply(ok, 2);
                         Serial.println(F("AT+CWQAP"));  return;}    // if series not set, then get time only, no run pending config
       
    //Step 1 /////// check Pending config ///return ((valid)) ((invalid))////// 
       lcd.setCursor(5,0); lcddot();; 
       url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/pending-config/field3"); url[0] += F(" HTTP/1.1\r\n");
       //url[2] = F("Connection: Keep-Alive\r\n"); 
       //url[3] = F("\r\n");    
             
       for (i=0; i< tryi; i++) {UpdateIOT1(1, true); SVRreply(); if (!c_ap || c_svr) {break;}}
       //if (!yesno || !c_svr) { return;}         // if not yes, return, if yes, start parameter exchange 
       if (yesno) {         // if yes, then start config procedure
       
   //Step 2///// Start Check password, return ((valid)), ((invalid)) ///////
       lcd.setCursor(6,0); lcddot();
       url[0] = F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/start/field4"); url[0] +=F(" HTTP/1.1\r\n");
       url[3]= F("{\"field4\":\""); url[3] += WebKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\nConnection: Keep-Alive\r\n\r\n"); 
             
        for (i=0; i< tryi; i++) {UpdateIOT1(1,true); SVRreply(); if (!c_ap || c_svr) {break;}}
        if (!yesno || !c_svr) {return;}
       
   
   //Step 3////// ask field 8 Set Tank KH, ////return ((7500))//////     
       lcd.setCursor(7,0); lcddot();  
       url[0] =F("GET /api/v1/KH/");url[0] += SERIES; url[0] += F("/field8"); url[0] += F(" HTTP/1.1\r\n");
       url[2] = F("Connection: Keep-Alive\r\n"); 
       url[3] = F("\r\n");
             
       for (i=0; i<tryi; i++) {UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;}}
       if (!yesno || !c_svr) {return;} 
       
       Fieldvalue=MSG.toInt(); if (Fieldvalue >0) {SetTankKH = Fieldvalue;} 
       if (SetTankKH == 1230) {testmode=1;} else {testmode=0;}   // set testmode from web if targetKH = 1.23
                      
   //Step 4////// ask field 13 Timer, return ((12))  //////////
       lcd.setCursor(8,0); lcddot();
       url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field13"); url[0] += F(" HTTP/1.1\r\n"); 
             
       for (i=0; i<tryi; i++) {1, UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;}}
        if (!yesno || !c_svr) {return;} 
       Fieldvalue= MSG.toInt(); if (Fieldvalue > 0) {Timer = Fieldvalue; Timerset();}
       
    //Step 4.1////// ask field 10 true tank KH for calibration  //////////
       lcd.setCursor(9,0); lcddot();;
       url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field10"); url[0] += F(" HTTP/1.1\r\n"); 
             
      for (i=0; i<tryi; i++) {UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;}}
        if (!yesno || !c_svr) {return;} 
        Fieldvalue = MSG.toInt(); if (Fieldvalue >0) {
          trueKH = Fieldvalue; trueKHflag=true;
        }   
   //Step 4.2 ////// ask field 5 mldKH, Dosing ? ml per 1 dKH, ////return ((350))/////
       lcd.setCursor(10,0); lcddot();  
       url[0] =F("GET /api/v1/KH/");url[0] += SERIES; url[0] += F("/field5"); url[0] += F(" HTTP/1.1\r\n");
             
       for (i=0; i<tryi; i++) {UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;}}
       if (!yesno || !c_svr) {return;} 
       
       Fieldvalue=MSG.toInt(); if (Fieldvalue >0) {mldKH = Fieldvalue;}   
   //Step 4.3 ////// ask field 6 maxml, maximum dKH (ml) per hour, ////return ((35))//////
       lcd.setCursor(11,0); lcddot();  
       url[0] =F("GET /api/v1/KH/");url[0] += SERIES; url[0] += F("/field6"); url[0] += F(" HTTP/1.1\r\n");
             
       for (i=0; i<tryi; i++) {UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;}}
       if (!yesno || !c_svr) {return;} 
       
       Fieldvalue=MSG.toInt(); if (Fieldvalue >= 0) {maxml = Fieldvalue;}
          
           
   //Step 6///// end of transaction, return ((yes)) //////////  
      lcd.setCursor(12,0); lcddot();
      url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/eot"); url[0] += F(" HTTP/1.1\r\n"); 
      
       for (i=0; i< tryi; i++) {UpdateIOT1(1, true); SVRreply(); if (!c_ap || c_svr) {break;}}
       if (!c_svr) {return;}
       
        
    //Step 7///// Ask new password, return new password ((k328)) or ((invalid))//////////
      //lcd.setCursor(4,0); lcd.print(F("        ")); 
      lcd.setCursor(13,0); lcddot();
       url[0] =F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field2 HTTP/1.1\r\n"); 
       url[3]= F("{\"field2\":\""); url[3] += WebKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\nConnection: Keep-Alive\r\n\r\n"); 
       
       char NewKey[6];      
        for (i=0; i<tryi; i++) {UpdateIOT1(1, true);  SVRreply(); if  (!c_ap || c_svr) {break;}}  //    return "3gS5" or "invalid"  without "yes" or "valid"
        if (!yesno || !c_svr) {return;}     
          
        MSG.toCharArray(NewKey, 6); 
                
   //Step 8///// Confirm new password, return ((valid)), ((invalid)) //////////   
       lcd.setCursor(14,0); lcddot();
       url[0] =F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field4 HTTP/1.1\r\n"); 
       url[3]= F("{\"field4\":\""); url[3] += NewKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\nConnection: Keep-Alive\r\n\r\n"); 
       
      for (i=0; i< tryi; i++) {UpdateIOT1(1, true); SVRreply(); if  (!c_ap || c_svr) {break;} }
       if (!c_svr) {return;} 
       strcpy(WebKey, NewKey); EPROMsave();
       
   }   // if (yesno) pending config
   
   // Step 9//// upload data ///////////////////////////////////////////////////////
   if (NewKHflag) {
      NewKHflag =false;   // reset New KH flag =0
      lcd.setCursor(15,0); lcddot();    
       
       url[0] = F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/meas"); url[0] +=F(" HTTP/1.1\r\n");
       url[3]= F("{\"field4\":\""); url[3] += WebKey; 
       url[3] += F("\", \"field22\":"); url[3] += TankKH;   
       url[3] += F(", \"field23\":"); url[3] += 1000;      // not upload TankKH for security.   TankPH/10; 
       url[3] += F(", \"field24\":"); url[3] += RefPH/10;  // for server to calculate PH life
       url[3] += F(", \"field25\":"); url[3] += RefKH;  
       url[3] += F(", \"field26\":"); url[3] += KHdos; 
       url[3] += F("}\r\n");
       
       url[2]=  F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\nConnection: close\r\n\r\n"); 
       
       for (i=0; i< tryi; i++) {UpdateIOT1(1, true); SVRreply(); if (!c_ap || c_svr) {break;}}
       
    }  // if NewKHflag
    
    Serial.println(F("AT+CIPCLOSE"));ESPreply(ok, 2); Serial.println(F("AT+CWQAP"));
}


///// upload data /////////
void UpdateIOT1(int IP, bool alive) {
  char prompt[] = ">"; char ok[] = "OK"; 
      byte i =0; byte ms=20;      
    
    ///////// connect Server /////////  
         
    for (i=0; i< 3; i++ ) {   //lcd.setCursor(15, 0); lcd.print(i); delay(1000);
      if (i!= 0 || alive) {   // if keep-alive (true) then check connection status, if not then connect server
          ESPstatus(); if (yesno) {break;}  // if connect to server, then break for loop
          if (!c_ap) {connectWiFi();}
          if (!c_ap) {return;}  // if fail to connect AP then return
          } // if alive
          
          // c_ap true but not c_svr 
          
          Serial.println(F("AT+CIPSTART=\"TCP\",\"server.aquawiz.net\",80")); 
          cleanbuffer();     // delay sometime to wait server reply   
       }  // for i
       
       if (!c_svr) {lcd.setCursor(0, 1);lcd.print(F("   WWW Fail !   ")); delay(2000); return;}  // if fail to connect server then return
   
            
       //////// CIPSEND command send url length   //////////  
       //lcd.setCursor(15, 0); lcd.print(">"); delay(100); 
       
           for (i=0; i<4; i++) { delay(ms);    /// url[0]-url[3]
               Serial.print(F("AT+CIPSEND=")); Serial.println(url[i].length());   
               ESPreply(prompt, 5); if (yesno) {Serial.print(url[i]);}
               if (i < 3) {ESPreply(ok, 5);} 
               if (!yesno) {break;}
           }  // for i
}     
  

/////////// Control KH ////////////
void ControlKH() {
      
      static int Period2, KHold, KHnew, KHdis;
      int userhour, Period, Period1, dummy, KHnew1, KHdis1, testTankKH, KHdir=1, KHspc=100;
      int series = String(SER).substring(7).toInt(); //; KH1-00-00065; become 00065 ; int series_int = series.toInt();
      byte Pump1rev = Pump1on + 30, Pump2rev = Pump2on + 30 ; // Pump1rev = Pump1on + 30, Pump2rev = Pump2on + 30 ;
      bool Halt2;
      
    lcd.clear();  DisplayPHvalue();
  
      
    if (!Halt) {Halt2 = false;}          //  if Halt = false, no halt, // another Halt that can penetrate while {
    else { Halt = false; Halt2 = true;}   //if Halt = true, halt before, then reset Halt and keep StepId, but set another Halt2 to penetrate while
         
    while (!Halt) {     // loop until Halt = true, hit any key
        
        if (!Halt2) {    // if not Halt, then check timer to determine measure or not
          if (now() < 7200) {SyncWeb();lcd.clear();} // just power up, now=0
          Period = (60 - minute()) * 60 - second(); time_t t = now(); 
          if (Period < 2100) t += 3600;        // if time left (Period) < 2100sec, 35 min, then use next hour (+1) timer.
          userhour = hour(t);               // check userhour and userweekday boundary
          if (userhour%Timer1==0){  // check timer is on time?
             if (Air_off > Air_on) {   // check timer is sleep time?
                if (userhour < Air_off && userhour >= Air_on) {StepId = 1; sleep=false;} else {StepId=11; sleep=true;}
             } // if Air_off > Air_on
             else if (Air_off < Air_on) {    
                if (userhour >= Air_off && userhour < Air_on) {StepId = 11; sleep=true;} else {StepId=1; sleep=false;}
             } // else if Air_off < Air_0n
             else {StepId = 1; sleep=false;}  // Air_off=Air_on
          } // if Timer1   
          else {StepId= 11; sleep=true;}  // Timer1 not on, then it is sleep         
        } // if !halt2   
        else { Halt2 = false;}      // if Halt, keep StepId, and reset Halt to false, if StepId = 12 then recalculate Period
        
        if (trueKHflag) {StepId = 1; KHrep=false; sleep=false;}  // if KH calibration, than force start from step1, and reset KHrep 
        
        delay(500);dac.setVoltage(dacKH(TankKH), false); delay(500);
        
        for (int z = StepId; z < 14; z++) {
        switch (z) {
        case 1:
           ProcStep(F(" A2 "), Pump2rev, 3); // 45 R>  empty right vessel to tank  turn right => 2 low, 3 onPWM
           break; 
         
        case 2:
           ProcStep(F(" A3 "), Pump1rev, 1); // 50 L<   empty left vessel to reference   turn left => 0 high, 1 onPWM
           break;  
        
        case 3:
           ProcStep(F(" A4 "), Pump2on, 2); // 31  R<  fill right vessel from tank   turn left  => 2 high, 3 onPWM
           break; 
        
        case 4:           
           ProcStep(F(" A5 "), Pump1on, 0); // 46  L>  fill left vessel from reference  turn right  => 0 low, 1 onPWM
           break; 
        case 5:     
           Ref_PH(StablePH); // 900  measure Ref PH in left vessel
           break;    
        case 6:  
           ProcStep(F(" A7 "), Pump1rev, 1); // 50 L<   empty left vessel to reference  turn left => 0 high, 1 onPWM
           break;  
        case 7:
           ProcStep(F(" A8 "), Pump1on-3, 4); // 70  C<   move right to left  turn left => 4 high, 5 onPWM, -3 is to keep a little water in chamber to conduct electric leak from tank
           break;  
        case 8:
           wdt_enable(WDTO_1S); // enable watch dog
           if (!KHrep) {          // first measure, no need to repeat measure
              KHold = TankKH;
              Tank_PH(RefPHtime);  // measure Tank KH
              KHnew= tempKH;
              KHdis = KHnew-KHold; KHdis = abs(KHdis);  // calculate distance of KHnew to KHold
              if (KHdis > KHspc && testmode==0) {KHrep = true; } else {NewKHflag =true; TankKH=tempKH; } // if KH distance > 0.1 out of spec and not in testmode, then repeat measure
           }  else {               // KHrep=true, need to repeat measure
              Tank_PH(RefPHtime); // S8: 60    measure Tank PH
              KHnew1 = tempKH; KHdis1 = KHnew1-KHold; KHdis1 = abs(KHdis1);
              if (KHdis < KHdis1) {TankKH = KHnew;} else {TankKH = KHnew1; }     // TankKH default = KHnew1
              
              KHrep=false; NewKHflag =true; 
           }  // else !KHrep
          
           if (trueKHflag && !KHrep) {                  // input new thank KH to calibrate ref_KH, must mix once to get stable measure.       
              RefKH = trueKH - TankKH + RefKH;
              TankKH = trueKH;
              trueKHflag = false;      
              EPROMsave();   // save RefKH to ROM 
           } // if trueKHflag
           wdt_reset(); 
           
           if (!KHrep) {delay(500);wdt_reset();delay(500); wdt_reset();KHOrelay();} 
           else {lcd.setCursor(12, 0);lcd.print(F("Rep"));} // show Rep on LCD  /// 
           wdt_disable(); // disable watch dog 
           break; 
        case 9:
           ProcStep(F("A10 "), Pump1rev, 5); // 45 C>  move left vessel to right   turn right => 4 low, 5 onPWM
           break;   
        case 10:
           Hmark=digitalRead(waterlevel); // check the water level of high mark on reference box
           ProcStep(F("A11 "), Pump1on+10, 0); // 46  L>  fill left vessel from reference, add 10 sec water 
           Lmark=digitalRead(waterlevel);  // check the water level of low mark on reference box
           ProcStep(F("A11 "), 10, 1); // 50 L<   empty left vessel to reference, remove 10sec water
           if (KHrep) {z=4;}  // if need to repeat measure, re-direct z=4, actually z++ = 5, Ref_PH step
           break;   
        case 11: 
           wdt_enable(WDTO_1S); // enable watch dog 1 second
           lcd.setCursor(8, 0); lcd.print(F("A12 "));
           ///////// use SERIES to spread web sync section, get 0-99 section  
           Period = (60 - minute()) * 60 - second(); // calculate total remaining time ///
           dummy = Period-1800; if (dummy < 0) dummy = 0;  // dummy time let period = 30min if period > 30min
           
           Period1 = (Period-dummy-300)/100 * (series%100) + dummy;  // keep Period1 in step 12 > 5min 
           if (Period1 < 0) Period1 = 0;
           Period2 = Period - Period1;  // period2 for A12
           
           if (testmode ==1) {
             Period1 = 300/50 * (series%50);  // in testmode, send data before xx:30, and spread 50 serial# in 5 min(300 sec)   
           }
           
           Count_S (Period1, true); 
           wdt_disable(); // disable watch dog 
           break;
       case 12:      
           wdt_disable(); // disable watch dog 
           SyncWeb();  ////// update data, password and get parameter from web
           ///////// adjust water level of reference box ////////
           if (!sleep && testmode==0) {
             if (Hmark && Lmark) {Turnsec(5,4);}   // all lower than level, move 3 sec water from right to left chamber
             if (!Hmark && !Lmark) {Turnsec(4,4);} // all higher than level, move 3 sec water from left to right chamber
           }  // if not sleep
           ///////////////////////////////////////
           if (trueKHflag) {z = 0; KHrep=false; sleep=false;lcd.clear();}  // if KH calibration, than force start from step1, z=0+1, and reset KHrep 
           break; 
       case 13:  
           wdt_enable(WDTO_1S); // enable watch dog 1 second
           lcd.clear(); 
           lcd.setCursor(8, 0); lcd.print(F("A13 "));
           
           Period = (60 - minute()) * 60 - second();
           if (Period > Period2) Period = Period2; 
           
           if (testmode ==1) {
             if (minute() >= 30) {Period = (60 - minute()) * 60 - second();
             } else {Period = (30 - minute()) * 60 - second(); }// wait xx:30 to start another run
           }  // if testmode
           
           Count_S (Period, true);  
           wdt_disable(); // disable watch dog  
           break; 
       }  // switch 
       if (Halt) {StepId = z; break;}  // ; record stepID break for
    }  // For loop
       if (Halt) {break;}    //  break while to MENU page
       StepId = 1;        // reset StepId =1 to start from 1st step, should be put after break
    }  // while Halt   // 
       wdt_enable(WDTO_1S); // enable watch dog 1 second
       digitalWrite(Airpump, LOW); wdt_reset();   // turn off air pump
       wdt_disable(); // disable watch dog  
    }    //void


void Count_S (long t, bool hit_botton) {
  unsigned long C_start, Elapse=0; bool LCDlight=true; int LCDtime=180, Elapse_s=0, count ; //int count; char buf[10];
  
  if (t < 0) t=0;
    
  while (Elapse_s < t) {    // count time
      C_start = millis();
      //if (cdown) {count = t-Elapse_s; lcd.setCursor(8, 1);lcd.print("     ");lcd.print(count);lcd.print("  "); }        // count to zero
      ReadPHvalue(); DisplayPHvalue();  
      if (Elapse_s > LCDtime) {lcd.noBacklight(); LCDlight=false; } //no backlight
      if (ReadButton() != 0 && hit_botton) {
          if (!LCDlight) {lcd.backlight(); LCDlight=true; LCDtime = Elapse_s + 180;}
          else {Halt = true; break;} //  break while loop 
       }  //if
      Elapse += (unsigned long)(millis() - C_start); Elapse_s = Elapse/1000;  // Count in second  
      wdt_reset(); // reset watch dog timer
  }  // while 
      lcd.backlight();
  
}
void delaycdown (long t, bool waterl) {    // accurate to 0.1 sec
  unsigned long C_start, Elapse=0; int Elapse_s=0, count ;
  if (t < 0) t=0;
  while (Elapse_s < t) {    // count time
      C_start = millis();
      count = (t-Elapse_s)/10; lcd.setCursor(11, 1);lcd.print(count);lcd.print(F("     "));         // count to zero
      Elapse += (unsigned long)(millis() - C_start); Elapse_s = Elapse/100;  // Count in 0.1 second  100 msec
      if (waterl && digitalRead(waterlevel)) {break;}
      wdt_reset(); // reset watch dog timer
  }  // while 
}
///////////// 2nd Gen -- A4950 driver ////////
void ProcStep(String i, int t, byte turn) {
  wdt_enable(WDTO_1S);  // enable watch dog timer 1 sec
  lcd.setCursor(8, 0); lcd.print(i); 
  analogWrite(relay[turn], onPWM);  // turn on pump
  Count_S (t, false);              // last t sec, no halt allowed, no dose allowed
  analogWrite(relay[turn], 0);   // turn off pump
  wdt_disable(); // disable watch dog timer
  delay (1000);
}

//////////////////////
void Ref_PH(int t){
  int ph, maxi=100;
  wdt_enable(WDTO_1S);  // enable watch dog timer 1 sec
  lcd.setCursor(8, 0); lcd.print(" A6 ");
  digitalWrite(Airpump, HIGH);  // turn on air pump 
  Count_S (t, true);   // allow hit button to halt, not allow dosing
  if (!Halt) { RefPH = 0; for (int i=0; i<maxi; i++) {ReadPHvalue(); RefPH = RefPH + total; wdt_reset();} }// for // if
  wdt_disable(); // disable watch dog timer
}
/////////////////////
void Tank_PH(int t){
  int num10_7 = 3010; int PH10_7 = PH10-PH7; int num5 = 5;int i, ph;  // PH10-PH7 = 53000
  int maxi=100;
  wdt_enable(WDTO_1S);  // enable watch dog timer 1 sec
  lcd.setCursor(8, 0); lcd.print(F(" A9 ")); 
 
    Count_S (t, false);   //wait time, no hit button and Halt allowed, no dose allowed
    TankPH = 0; for ( i=0; i<maxi; i++) {ReadPHvalue(); TankPH = TankPH + total;wdt_reset();}  // for
    
    ////// calculate tempery tank KH tempKH = RefKH - (RefPH - TankPH) * 20 ///////
    //tempKH = RefKH - (num10_7 * (RefPH-TankPH)) / (PH10_7) /num5;
    tempKH = (num10_7 * (TankPH-RefPH)) / (PH10_7) /num5;
    tempKH = RefKH + (float)tempKH * 1.30;  // correction factor to fit salifert KH result
    DisplayPHvalue();  // display TankKH value  
       
    digitalWrite(Airpump, LOW); // turn off air pump
    wdt_disable(); // disable watch dog timer
}

void KHOrelay() {     
   ///////// KH control output ///////
   int displayKH = TankKH;
   int doskh,dos01sec,dosml,maxkh, abs_doskh, kh_dis;
   wdt_enable(WDTO_2S);  // enable watch dog timer 2 sec
   if (trueKHflag){displayKH = trueKH;}
   DisplayPHvalue();
   
         // sleep will junp to step 11, no chance to do KHOrelay
         ///////// always KH output ///////////////////
                  doskh = SetTankKH-displayKH; kh_dis = abs(doskh);
                  if (kh_dis > 1000) kh_dis = 1000; // keep 0 sec to 10 sec
                  abs_doskh = kh_dis*10; // precision to 0.1 sec
                  digitalWrite(KHO, LOW);wdt_reset();delay(1000);   // high/low 1 sec as triger
                  digitalWrite(KHO, HIGH);wdt_reset();delay(1000);
                  
                  digitalWrite(KHO, LOW);wdt_reset();// high for send dKH delay
                  wdt_disable(); // disable watch dog timer
                  delay(abs_doskh);   // on 1 sec means 0.1 dKH
                  wdt_enable(WDTO_2S);  // enable watch dog timer 2 sec
                  digitalWrite(KHO, HIGH);wdt_reset();// KH relay off
                  if (doskh < 0) {    // signal to represent negative value
                     delay(1000); // keep low 1 sec
                     digitalWrite(KHO, LOW);wdt_reset();// high one sec
                     delay(1000);  
                     digitalWrite(KHO, HIGH);wdt_reset();// keep low for rest
                  }  // if doskh < 0

         KHdos=0; // initialize KHdos=0
         if (SetTankKH > displayKH) {                           // using dosing pump
                  digitalWrite(Dospump, HIGH); wdt_reset();// KH relay on
                    if (maxml > mldKH) {maxml = mldKH;}  // limit <= 1 dKH dosing per hour   
                    maxkh=(long)maxml*1000L/(long)mldKH; //dKH*1000L L means Long
                    doskh = SetTankKH-displayKH; 
                    dosml = (long)doskh*(long)mldKH/1000L; 
                    if (dosml > maxml) {dosml=maxml;doskh= maxkh;} // calculate maximum dose second
                       dos01sec =(long)dosml*(long)hundccsec/10L; // in 0.1 sec  
                    if (dos01sec > max01sec) {dos01sec= max01sec; 
                       doskh=(long)max01sec*10000L/(long)hundccsec/(long)mldKH;}  // maximum 60 sec = 1 min dosing time
                       //doskh=max01sec/hundccsec*100/mldKH;}  // old version Wrong function, maximum 60 sec = 1 min dosing time
                  lcd.setCursor(8, 0);lcd.print(F("Dos ")); lcd.print((float)doskh/1000.0,2);
                  delaycdown(dos01sec, false); 
                  digitalWrite(Dospump, LOW); KHdos=doskh;   // relay off
                  wdt_reset(); 
          } // if setTankKH > displayKH 需要dose KH
         digitalWrite(KHO, HIGH); digitalWrite(Dospump, LOW);wdt_reset();  // reset all output I/O
   delay(500); 
   //lcd.setCursor(8, 0); lcd.print(F("KH2PH   ")); 
   wdt_reset();
   ////// KH to PH output //////////
   delay(500);dac.setVoltage(dacKH(displayKH), false); wdt_reset(); 
   delay(500);// delay to avoid I2C interrupt
   lcd.setCursor(8, 0); lcd.print(F("        "));
    wdt_disable(); // disable watch dog timer
   } 


////////////////// Sub Read and smooth PH value by numReadings = 100 times
void ReadPHvalue() {
   //int num10_7=3010; int num7=7000; 
   int maxi=1000;
   total = 0;
   for (int i=0; i< maxi; i++) {total = total + analogRead(A0);wdt_reset();}
   ////// total value //////////}
   total = (float)total/100.0 + 0.5;
   //PHvalue= num7 + num10_7 * (total-PH7) / (PH10-PH7); 
} 


//////////////////// Display PH value on lcd
void DisplayPHvalue(){
     //static unsigned long Countstart1, Count1; static int diff1, old1;  
     static bool adjflag; char buf[18]; 
     int displayKH = TankKH, total_10;
     
     /////// print KH  ///////
     lcd.setCursor(0, 0);lcd.print(F("KH ")); 
     
     if (trueKHflag){displayKH = trueKH;}// display true KH value after input KH calibration

     if (displayKH > 0 ) {   
        if (displayKH < 10000) {lcd.print((float)displayKH/1000.0, 2);} 
        else {lcd.print((float)displayKH/1000.0, 1); lcdspace1();}    
     }  // if displauKH > 0
     
     lcd.setCursor(15, 0);if (testmode==1) {lcd.print(F("T"));}
      
      /////// print time, probe lifetime (LF) and "change probe" ///////
      //if (now()%(36000/time_adj) <20) { if (adjflag==false) {adjustTime(sign(time_adj)); adjflag=true;}}   // add 1 sec after (3600/time_adj) second.
      //else {adjflag=false;} 

      
      total_10=total/5; // Change PH probe spc to total_10 < 100, total < 50. total_10=total/5 from total_10=total/10
      if (testmode==0 && total_10 > 100) {total_10 = 100;} // display 100 if LF > 100
      lcd.setCursor(0, 1);  lcd.print(F("LF ")); lcd.print(total_10);lcdspace2();
      lcd.setCursor(8, 1);if (Hmark && Lmark) {lcd.print(F("H  "));} else if (!Hmark && !Lmark) {lcd.print(F("L  "));} else {lcdspace3();} 
      lcd.setCursor(11, 1);lcdprintDigits(hour());lcd.print(":");lcdprintDigits(minute());
      
      
      //sprintf(buf, "LF %-3d    %02d:%02d ", total_10, hour(), minute());lcd.print(buf);
      //sprintf(buf, "LF %-3d  %02d:%02d:%02d", total_10, hour(), minute(), second());lcd.print(buf);
          
          //if (total_10 > 100) {lcd.print(buf); }//////// print time ////////
          //else {
          //     if (second()%3) {lcd.print(buf); }//////// print time //////// 新版不顯示 chnange probe
          //     else {lcd.print(F("--Change Probe--"));}   // flash 
          //} // else tota_10 >100
          
      //}   // if !cdown --- not count down
      
  }  // void
///////// lcd print time digit ///////
 void lcdprintDigits(int digits){
  if(digits < 10) lcd.print("0");
  lcd.print(digits);
}
int dacKH (int kh) {
  if (kh<0) {kh=7000;}
  int dac = 4565.0-(float)kh/3.04;   //dac = kh*4096/14000 = tankkh/3.418
  if (dac <0){dac =0;} if (dac > 4095){dac=4095;}
  return dac;    
}

int sign(int x) { return (x>0) - (x<0); }

/*
void lcdPH() {
    lcd.setCursor(11,1); lcd.print(total);   lcd.print("    "); // Refresh total read value
    lcd.setCursor(10,0);  if (PHvalue < 10000) {lcd.print(" ");}lcd.print((float)PHvalue/1000.0, 2);  // continue refresh PH value 
}
void CheckPH() {
        int x=0;
        while (x == 0) {           //  if no button pressed then loop
            x = ReadButton();      // read button value:
        ReadPHvalue(); lcdPH();
        } //while
}
*/
////////////// Convert KH to PH format output through DAC ////
void KH2PH() {
     ////////// KH to PH output
     int Count;   // start count      
     int p,index, x=2;
     lcd.clear(); lcd.print(F("KH to PH output "));
     lcd.setCursor(0,1);lcd.print(F("KH -> PH = 7.0  "));
     delay(1000);
     p = Adj(p,3, 2, 1, 0, 4);
     delay(500);dac.setVoltage(dacKH(7000), false);delay(500);

     ////////// Fill reservoid ///////
     lcd.clear(); 
     lcd.setCursor(0,1);lcd.print(F("Water Box       "));
     delay(1000); index=0;
     index = Adj(index, 2, 0, 1, 0, 3);
     
}

////////////// initial setting SERIES number ////////
void AdjSER() {
   int p=0; String S=F("KH1-00-00");
   lcd.clear();
   lcd.setCursor(2,1);lcd.print(SER);
   p= Adj(p,9, 0, 1, 11, 0);S += p; 
   p= Adj(p,9, 0, 1, 12, 0);S += p;
   p= Adj(p,9, 0, 1, 13, 0);S += p;
   S.toCharArray(SER, 13);
   lcd.setCursor(0,1);lcdspace2();lcd.print(SER);delay(2000);
   setSER = 0; 
   EPROMsave(); // save SERIES and reset setSER
}
///////// general adjust function /////////
int Adj (int val, int maxi, int mini, int stepi, int row, int item) {
     int valold,Count; int r;
     int truecc=100; int oldhundccsec=hundccsec; // prepared for item 6
     
     int x = ReadButton();    // ReadButton
     while (x != 5) {         //  if select button not pressed then loop 
         x = ReadButton();
     if (x==3){val = val+stepi;}      // if up buttom pressed then add 1 digit
     if (x==2){val = val-stepi;}      // if down button pressed then minus 1 digit
     //////// way to change value ////////////// 
     if (item==6) {hundccsec=oldhundccsec*100/val;
                   if (hundccsec < 30) val--;if (hundccsec > 300) val++;}
     else {if (val > maxi) val = maxi; if (val < mini) val = mini;}
     //////// execution item //////////////
         if (item==1) {if (x==3){Turnsec(0,1);} if (x==2){Turnsec(1,1);}} // adjust pump
         if (item==2) {if (x==3){Turnsec(2,1);} if (x==2){Turnsec(3,1);}} // adjust pump 
         if (item==4) {if (x==3) {val=3; dac.setVoltage(dacKH(10000), false);delay(500);} 
                       if (x==2) {val=2; dac.setVoltage(dacKH(7000), false);delay(500); } }   
         if (item==7) {if (x==2){val=0; lcd1[1].clear();lcd1[val].print(F("LCD 0"));} 
                       if (x==3){val=1; lcd1[0].clear();lcd1[val].print(F("LCD 1"));}} // adjust lcd address  
                           
     //////// blink item /////////////////
     Count= millis()/200; 
     if (Count%3 != 0 || x==5) {         // flash on period 
         if (item==0) {lcd.setCursor(row,1);lcd.print(val);}
         if (item==4) {lcd.setCursor(11,1); if (val==2) {lcd.print(F("7.0 "));} 
                                            if (val==3) {lcd.print(F("10.0"));}}
         if (item==3) {lcd.setCursor(0,0); lcd.print(F("No  Fill  Refill"));
             if (val==2 && x==5) { x=4; // ReFill reservoid
                    ////// 抽水 /////
                    lcd.setCursor(0,1);lcd.print(F("Water out       "));
                    digitalWrite(relay[3], HIGH);digitalWrite(relay[0], HIGH);digitalWrite(relay[5], HIGH); 
                    delaycdown(13000, false); digitalWrite(relay[3], LOW);digitalWrite(relay[0], LOW);digitalWrite(relay[5], LOW);
                    val=1; r=4; x=5;} // continue to go next val for water fill
            
             if (val==1 && x==5) { x=4;    // Fill reservoid
                    ////// 進水 //////
                    lcd.setCursor(0,1);lcd.print(F("Water in        "));
                    digitalWrite(relay[2], HIGH); delaycdown(13000, true);;  // check waterlevel
                    digitalWrite(relay[2], LOW);   // turn off pump
                    //lcd.setCursor(0,1);lcd.print(F("Out tube        "));
                    digitalWrite(relay[3], HIGH); delaycdown(Pump2on*25, false); 
                    digitalWrite(relay[3], LOW);
                    val=0; x=5; StepId=1;} // break to menu, and startover  
         } 
         if (item==5) {lcd.setCursor(0,1); lcd.print(F("No   10cc   Yes "));
             if (val==1 && x==5) { x=4;  // change x not break by while loop
                    lcd.setCursor(r,1); lcd.print(F("10cc"));  // keep 10cc on LCD 
                    digitalWrite(Dospump, HIGH); delaycdown(hundccsec, false);digitalWrite(Dospump, LOW);} 
         } 
         if (item==6) {lcd.setCursor(0,1); lcd.print((float)val/10.0,1); lcd.print(F(" cc "));
                       lcd.print((float)hundccsec/10.0,1);lcd.print(F(" sec "));}  
                                                    
       }    // if Count      
     ///////// flash off period ///////////
     if (Count%3 == 0 && x!=5) {          // flash off period  
        if (item ==0) { lcd.setCursor(row,1); 
                        if (val < 10) lcdspace1();}
        if (item==3) {if (val==0) {r=0;} else if (val==1) {r=4;} else {r=10;}
                      lcd.setCursor(r,0);lcdspace4();if (val==2){lcdspace2();}; } 
        if (item==4) {lcd.setCursor(11,1);lcdspace4();}    
        if (item==5) {if (val==0) {r=0;} else if (val==1) {r=5;} else {r=12;}
                      lcd.setCursor(r,1);lcdspace4(); } 
        if (item==6) {lcd.setCursor(0,1);lcd.print(F("       "));}  
           
      }    // else Count%3 
      
   
     }   // while
     //if (item==0) {lcd.setCursor(row,1); lcd.print(val);}
     //if (item==6) {lcd.setCursor(0,1); lcd.print((float)val/10.0,1);}
     if (item==7) {lcd = lcd1[val]; lcd.print(F(" OK"));} // assign lcd=lcd1[lcdadr]
     return val;
} 
void lcdspace4() {lcd.print(F("    "));}
void lcdspace3() {lcd.print(F("   "));}
void lcdspace2() {lcd.print(F("  "));}
void lcdspace1() {lcd.print(F(" "));}
void lcddot() {lcd.print(F("."));}
//////////////// Adjust water level in vessel ////////////
void AdjLevel() {
     int Count,index,x, r;
     if (StepId==5 || StepId==11){     // if initial setup, bypass level adjust, do dosing calibration 
     ////////////// Adjust left vessel level ////
     lcd.clear(); lcd.print(F("Left Tube Level "));
     delay(1000);
     Pump1on = Adj(Pump1on, 120, 0, 1,0, 1);
     
     ////////////// Adjust right vessel level ////
     //// pump2on 35 sec for 2meter long tube, 15 sec for 0 meter long tube///////
     lcd.setCursor(0, 0); lcd.print(F("Right Tube Level"));
     delay(1000);
     Pump2on = Adj(Pump2on, 120, 0, 1,0, 2);
     } // if StepId

     
     ////////////// Calibration 10cc ////
     if (hundccsec<=30){hundccsec=30;} if (hundccsec>300){hundccsec=300;} 
     lcd.clear(); lcd.print(F("Calibration 10cc"));
     delay(1000);
     index = Adj(index, 2, 0, 1, 0, 5);
    
     ////////////// Input Calibration true cc ////
     if (index==2) {
     lcd.clear(); lcd.print(F("True 10cc =     "));
     delay(1000); int truecc=100; 
     truecc = Adj(truecc, 0, 0, 1, 0, 6);
     } // if index
     
     EPROMsave();   //Write new Pump1on, Pump2on to EEPROM 
     
}
void Turnsec(byte i, int t) {
     digitalWrite(relay[i], HIGH); delay (t*1000); digitalWrite(relay[i], LOW); 
} 

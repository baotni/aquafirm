/////////////////////////////////////////////////////
//Hardware: DS3231 timer, KH input 4.7v diode+200 ohm
//Firmware function:
//  1. PID to calculate output for current value and last value
//  2. Control mode: Large and small CaRx modes
//  3. PH mode: maintain constant PH value and flow minutes to add KH
//  4. Wifi function
//  5. Sync with server and database
//  6. Serial number assigned after 1st power up
//  7. upload minimum PH value for CO2 empty warning
//  8. Sync time depend on serial number
//  9. start from menu
//  10. PH output through DAC 
//  11. using tube bending valve
//  12. maximum CO2 adjust depend on error
//  13. add upper lower value limit for PHvalue to prevent -30 PHvalue after power failure/recover
//  14. get Date from aquawiz server
//  15. Wifi transfer data: CRA server == KHA client
///////////////////////////////////////////////////////////////////////////////////////////   
#include <EEPROM.h>  // save parameter to EEPROM
#include <Wire.h>  // 1602 LCD I2C
#include <LiquidCrystal_I2C.h> //1602 LCD I2C
#include <TimeLib.h>
#include <Adafruit_MCP4725.h> // DAC for KH2PH output
LiquidCrystal_I2C lcd(0x27, 16, 2);
LiquidCrystal_I2C lcd1[2]={LiquidCrystal_I2C(0x27, 16, 2),
                           LiquidCrystal_I2C(0x3F, 16, 2)};


byte resetEPROM = 0  ;  // 0: not reset; 1: reset all;  2:reset some                                                                           ;
byte testmode= 0;  // 0: product 1: test mode+wifi 2: test mode w/o wifi
byte localhost =0; // 0: to AWS website 1: to localhost 192.168.0.27

char SER[13] = "CA1-00-00000";
char DEV[13] = "KH1-00-00000";
int setSER=1;
int lcdadr = 0;
char SSID[33] = "Tayler_Home"; 
char PASS[33] = "0922473267";   
char WebKey[6] = "gySb8";
int timezone = 8;
int wifichannel = 1;  // for CRA 
int time_adj = 200;   // adjust time in sec/hour time adjust factor
bool online = true;


//////// web parsing ///////
String url[4] ={"","","",""} ;   //url[0] - url[3]
String MSG ="";

///////// wifi flag ////////
bool newssid = false;   // change AP ssid and password flag
bool c_ap = false;    // connection to AP ok or not
bool c_svr;           // connection to server ok or not
bool yesno;          // server reply ok or not for GET and POST


#define waterservo  15 // KH input jacket on/off
#define KHI  2 // KH input jacket on/off

#define CO2valve  13 // CO2 valve on off I/O
#define CO2valve1  12 // CO2 valve on off I/O
 
//const byte relay[6] = {10,11,5,3,9,6};  // 11,10,5,3,9,6 
const byte input[3] = {8,7,4}; 

// Structure Object for EEPROM
const int eeAddress = 0; // EEPROM start address

struct CAparameter{
  int setSER;
  int PH7;
  int PH10;
  int OpenAng;
  int SetCaPH;
  int CO2period;
  int Cmode;
  int TuneNow;
  int TunePast;
  int mldKH;
  int maxml;
  byte CO2pulse [24];
  int timezone; 
  int wifichannel; 
  int lcdadr;
  char WebKey[7]; 
  char SSID[33];
  char PASS[33];
  char SER[13];
  char DEV[13];
 };
// Timer parameter
byte StepId= 1;
byte Step;
bool Halt = false;
byte thr; // carry CO2 pulse hour to next hour
byte StartNewBubble = 0; // Time to xx:45, and start new bubble procedure 
int Sync_sec = 2700; // wifi sync time setting;
bool Syncyet = false; // wifi sync flag;
bool NewOpenAngFlag = false; // manual setting CaRx flow openangle flag
bool FlushFlow = false; // Flush water flow tube per day
byte flush_hr = 23;

byte CO2pulse[24] = {20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20};  // co2 pulse count timer 0~23 oclock
//byte CO2pulse [24] = {60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60};  // co2 pulse count timer 0~23 oclock
//byte CO2pulse [24] = {0,0,0,0,0,0,0,10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,0,0};  // co2 pulse count timer 0~23 oclock
//byte CO2pulse [24] = {240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240};  // co2 pulse count timer 0~23 oclock
//byte CO2pulse [24] = {120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120};  // co2 pulse count timer 0~23 oclock

int CO2period=30;  //co2 off time = 4sec
byte CO2new[24]; // define new CO2 pulse setting for current hour
int dKH[24];  // record dKH send from KH controller for [0] current [1] last [2] last-last KH difference to target KH
int Cmode=0;
int TuneNow=1;
int TunePast=1;
int mldKH=50; // CO2 pulse count to increase 1 dKH
int maxml=20;  // maximum CO2 fix
int onPWM = 255;

////// PH related parameter /////
//#define Range 4038; // define PH10-PH7 range of normal PH probe. Bad one will show smaller range
int PH7=2560, PH10=7937;          // PH calibration parameter
int SetCaPH=4000, OpenAng=95,PHmin = 10000, PHminSync;
long total=0, PHvalue=0;    // PH parameter
bool newdKH;

// Lcd keypad Buttom setup
  byte Menu = 1; 
  bool Action = false;
  //bool Start =true;   // Device start, Program start, 

////// DAC setup //////
Adafruit_MCP4725 dac;  // far away from LCD setup
  
////////////////////////////////////
void setup() {
  byte i, p;  
  
// LCD setup
    lcd1[0].begin();lcd1[1].begin();lcd.begin();  
    // wifi i/o setup  
    Serial.begin(115200); // ESP8266 baur rate 115200               
  
// structure variable P 
  if (resetEPROM==1) {EPROMsave(); }   // Reset EPROM for new PCB board or parameter structure
  CAparameter P;         //Variable P to store custom object read from EEPROM.
  EEPROM.get(eeAddress, P);
    setSER = P.setSER;
    PH7 = P.PH7;
    PH10 = P.PH10;
    OpenAng = P.OpenAng;
    SetCaPH= P.SetCaPH;
    CO2period = P.CO2period;
    Cmode = P.Cmode;
    TuneNow = P.TuneNow;
    TunePast = P.TunePast;
    mldKH = P.mldKH;
    maxml = P.maxml;
    memcpy (CO2pulse, P.CO2pulse, sizeof(CO2pulse));
    timezone = P.timezone;
    wifichannel = P.wifichannel;
    lcdadr = P.lcdadr;
    strcpy (WebKey, P.WebKey);
    strcpy (SSID, P.SSID);
    strcpy (PASS, P.PASS);
    strcpy (SER, P.SER);
    strcpy (DEV, P.DEV);

// initialize value
    memcpy (CO2new, CO2pulse, sizeof(CO2new)); // copy CO2pulse array to CO2new array
    for (i=0; i<24; i++) {dKH[i] = 0;} // initiallize all dKH[hour] = 0;

//////// LCD display ////
    lcd = lcd1[lcdadr]; // set LCD address 
    lcd.clear(); lcd.print(F(" CaRx controller"));
    lcd.setCursor(0,1); lcdspace2();lcd.print(SER);
    
    if (resetEPROM !=0) {lcd.setCursor(0,1);lcd.print(F("Reset OK        ")); delay(2000);}     

    // DAC begin
    dac.begin(0x60);  

    // button setup
    for (i=0; i<=2; i++) {  pinMode(input[i], INPUT);}
  if (setSER==1) {   // 
     // LCD select i2c address ///
     lcd.clear();
     i = 1;
     while (i != 5) {         //  if select button not pressed then loop 
         i = ReadButton();
         if (i==2){lcdadr = 0; lcd1[1].clear();}      // if up buttom pressed then add 1 digit
         if (i==3){lcdadr = 1; lcd1[0].clear();}  
         lcd1[lcdadr].setCursor(0,0);lcd1[lcdadr].print(F("LCD setup")); 
     } // while    
     lcd = lcd1[lcdadr]; lcd.print(F(" OK"));
 } // if set SER

 // input I/O from KH controller
  pinMode(KHI, INPUT); 
  
// Relay digital output setup   
  //for (i=0; i<=5; i++) { pinMode(relay[i], OUTPUT); digitalWrite(relay[i], LOW);  } 
  pinMode(waterservo, OUTPUT); digitalWrite(waterservo, LOW);
  pinMode(CO2valve, OUTPUT); digitalWrite(CO2valve, LOW);
  pinMode(CO2valve1, OUTPUT); digitalWrite(CO2valve1, LOW);
  /// water servo off     
  FlowServo (waterservo,95, 2000); // close valve to acurate control opening
  FlowServo (waterservo,0, 2000); // close valve to acurate control opening
  //// CO2 valve on & Off test
  digitalWrite(CO2valve, HIGH);digitalWrite(CO2valve1, HIGH); delay(2000);// CO2 valve on
  digitalWrite(CO2valve, LOW);digitalWrite(CO2valve1, LOW);  // CO2 valve off
  if (setSER==1) {   
      // DAC check
      dac.setVoltage(dacKH(10000), false); // set dac = ph 10
    // check PH
    while (ReadButton()==0) {
    ReadPHvalue(); 
    lcd.setCursor(0, 1); lcd.print((float)PHvalue/1000.0, 2);  // print current PH 
    }
     
    // ESP 8266 wifi data transfer setup
    SyncWeb(); 
    lcd.setCursor(11, 1);lcdprintDigits(hour());lcd.print(":");lcdprintDigits(minute()); 
    delay(2000); // 
  
    //// Set serial number /////
    AdjSER(); 
    
    } // if setSER  
    dac.setVoltage(dacKH(7000), false);  // reset dac  
    
}
///////////////////////////
/////////Menu Loop//////////////
///////////////////////////
void loop() {
    unsigned long C_start= millis();   // start count 
    static unsigned long Elapse; int Count=Elapse/200;
    int menutime=100;
      switch(ReadButton()) { 
      case 3:  Menu ++; if (Menu > 4) {Menu = 1;} break;    //if down (increase)
      case 2:  Menu --; if (Menu < 1) {Menu = 4;} break;    //if up   (decrease)  
      case 5:  Action = true; break;    //if Enter (Action) 
      }
              
      // if not hit button for 30 sec, then auto run KH control      
     if (Count > menutime) {Menu = 5; Action = true;}    // idle 0.2 *100 = 20 sec 

     if (Count%3 != 0) {       // flash 0.4 sec on
     //////////// lcd print top menu  
     lcd.setCursor(0,0); lcd.print (F("  Wifi   Sync   "));
     lcd.setCursor(0,1); lcd.print (F("  Flow   Other  "));
     }  // if Count%3
     
   switch (Menu) {
    case 1:
        lcd.setCursor(0,0);
        if (Action) {WebServer(); }    
        break;
    case 2:
        lcd.setCursor(8,0); 
        if (Action) {lcd.clear(); lcd.setCursor(0,0); lcd.print(F("Sync    ")); 
                     lcd.setCursor(0,1); lcd.print(F("Web Data & Input"));  
                     if (testmode!=0) {GetKH(); newdKH=true;}   // force to getKH for testmode
                     SyncWeb();
                     }  
        break;
    case 3:
        lcd.setCursor(0,1);
        if (Action) {InputCO2();} 
        break; 
    case 4:
       lcd.setCursor(8,1); 
       if (Action) {CalibratePH();} 
        break;
    case 5:
        if (Action) {ControlPH();}
        Menu = 1;    // reset Menu to 1
        break;
    
    
     } // switch
     
        if (Count%3 == 0) {lcd.print (F("        "));} // flash 0.2 sec off
       Elapse += (unsigned long)(millis() - C_start); 
       Count = Elapse/200;     // 0.2 sec timer
       if (Action) {Elapse = 0;}   // back to menu start count time 
       Action = false;
  }   // loop



int ReadButton() {           // up, down, and select only return once
  
  byte State = 0; static byte lastbutton;        // initial State = 0
  
  if (digitalRead(input[0]) == 0) { State = 2;}  // read down button
  if (digitalRead(input[1]) == 0) { State = 3;}  // read up button
  if (digitalRead(input[2]) == 0) { State = 5;}  // read select button
   
  if (State == 0 || State > lastbutton) {     
     lastbutton = State; return State;}
  else { return 0;}
}

////////////////////////////
void CRAserver () {
  if (!APServer(2)) {lcd.setCursor(0, 1);lcd.print(F("   WiFi Fail !  ")); delay(2000);} 
  return;  // Setup CRA AP mode               
}  //void

/////////// ESP8266 Web Server start to input SSID PASS //////////
////////////////////////////
void WebServer () {
  char ipd[] = "IPD,";  char ok[] = "OK";
  if (APServer(1)) {               // Setup ESp8266 as AP mode and Server mode
       lcd.clear(); lcd.print(F("WiFi Aquawiz_KH "));
       ESPreply(ipd, 300); // wait 5 min for client to login in AP
       lcd.setCursor(0, 1); lcd.print(F("Web 192.168.4.1 "));}
  else { lcd.setCursor(0, 1);lcd.print(F("   WiFi Fail !  ")); delay(2000); return;}                 
  ////////// wait for client login ///////////////////////             
  ESPreply(ipd, 300); // wait 5 min for client to login in
   ///////// serve homepage ////////
     while(1) { 
       lcd.clear(); lcd.print(F("Wifi Setup Page "));
       serve_homepage();// send webpage 2 times
       ///////// wait for message come in /////////////////
       lcd.setCursor(0, 1); lcd.print(F("Key-in & Submit "));
       if (clientmsg()) {       // if client has key-in ssid, password
                 lcd.clear(); lcd.print(F("Connect WiFi    ")); 
                 Serial.println(F("AT+CWMODE=1")); ESPreply(ok, 2);  connectWiFi();    // escape AP mode to sta mode and force phone to find other ssid
                 lcd.clear();lcd.print(F("SSID :"));lcd.setCursor(0,1); lcd.print(SSID); delay(2000); 
                 lcd.clear();lcd.print(F("Password :"));lcd.setCursor(0,1);lcd.print(PASS); delay(2000); 
                 lcd.clear();lcd.print(F("Time Zone :"));lcd.setCursor(0,1);lcd.print(timezone); delay(2000); 
                 EPROMsave();break;    // save SSID and PASS to EPROM  
       }  // if clientmsg
   }  //while

}  //void

/////////// send home page to input ssid and pass ////////
void serve_homepage() {
   char prompt[] = ">"; int  urllength = 1091; byte i;  // client channel ID  //1091
   //////////////////////
   //delay(1000);  // wait for broser ready for homepage
   ////// content /////   
   for (i = 0; i <2; i++) {    // send 2 times home page  
    //////// CIPSEND command send url length   //////////
     delay(1000);  // wait for broser ready for homepage
     Serial.print(F("AT+CIPSEND=0,")); Serial.println(urllength); 
    //////// sent url to client ////////
     ESPreply(prompt, 2); if (yesno) {
        Serial.print(F("HTTP/1.1 200 OK\r\ncontent-type: text/html; charset=utf-8\r\ncontent-length: "));  //77
        Serial.println(F("984\r\nConnection: close\r\n"));  //29 //984
        Serial.println(F("<html style=\"background-color:powderblue;\"><head><script>ssid=\"\";pawd=\"\";time=\"\";function ST(f1) {var request = new XMLHttpRequest();ssid=\"ssid=\"+f1.ssid.value+\"&\";pawd=\"password=\"+f1.password.value+\"&\";time=\"time=\"+f1.time.value+\" \";request.open(\"GET\",\"ajax_inputs\"+ssid+pawd+time,true);request.send(null);alert(\"WiFi Settings saved in KH Controller\");window.location.assign(\"http://www.aquawiz.net\");return false;}</script><span style=\"font-family: arial,helvetica,sans-serif\"><h1 style=\"font-size:300%;\">KH Controller Wifi Setup</h1><form style=\"font-size:200%;\" onsubmit=\"return ST(this)\">WiFi:<br><input type=\"text\" id=\"ssid\" name=\"ssid\" style=\"font-size:100%;\"><br><br>密碼 Password:<br><input type=\"password\" id=\"password\" name=\"password\" style=\"font-size:100%;\"><br><br>時區 Time Zone: GMT +<br><input type=\"text\" id=\"time\" name=\"time\" style=\"font-size:100%;\"><br><br><input type = \"submit\" class=\"button4\" value=\"連線 Connect\" style=\"font-size:100%;\"></form></span></html>"));
        Serial.println(); Serial.println();
        }   // if prompt
      delay(100);
    } // for i 
}    
///////////////////////////////////////////////////////
bool clientmsg ()  {
    char ssid[] = "ssid="; char pass[] = "password=" ; char tim[] = "time=" ; //char api[] = "api=" ;
    char msg[49]; msg[0]='\0' ; int last_pos =0; float fl; //char aquawiz[]= "aquawiz"; 
   /////// get message ////////
  cleanbuffer();  // clean buffer before get new message
   //Serial.setTimeout(1000);
   while (!Serial.available()); {} // wait.....
  
    ////////////////// no settankKH, trueKH, Timer1 version //////////////
      if (Serial.find(ssid)) {last_pos = Serial.readBytesUntil('&', msg, 48); msg[last_pos] = '\0'; } else {return false;} 
           urldecode(SSID, msg); newssid = true;     
      Serial.find(pass); last_pos = Serial.readBytesUntil('&', msg, 48); msg[last_pos] ='\0'; 
           urldecode(PASS, msg); 
      Serial.find(tim); fl = Serial.parseInt(); 
           timezone = fl;    
    
      return true;
}
////////// Convert url %20 %23 ... to ASCII ////////// 
void urldecode(char *dst, const char *src) {
 char a, b;
  while (*src) {
    if ((*src == '+')) { *dst++ = ' '; src++;}
    if ((*src == '%') && ((a = src[1]) && (b = src[2])) && (isxdigit(a) && isxdigit(b))) {
       if (a >= 'a')  a -= 'a'-'A'; if (a >= 'A')  a -= ('A' - 10);  else  a -= '0';
       if (b >= 'a')  b -= 'a'-'A'; if (b >= 'A')  b -= ('A' - 10);  else  b -= '0';
       *dst++ = 16*a+b;  src+=3;  }  // if src == % 
    else { *dst++ = *src++; }
  }  // while
  *dst++ = '\0';
 
}
//////////clean serial buffer to accept new come in message ////////////////
 void cleanbuffer() {
  unsigned long Countstart= millis(); long timeout = 1000;
      while (millis()-Countstart < timeout) {
          if (Serial.available()) { Serial.read(); Countstart = millis(); } // if serial not available > timeout then end
      }  // while
 }

/////////// check ESP reply OK, IPD, /////////////////// 

void ESPreply(const char* com, int timeout)  {
   
   /////// get message ////////
    char buf[10] = {'\0'}; strcpy(buf, (char *) com);
    long TO = timeout*1000; 
    Serial.setTimeout(TO);   
         if (Serial.find(buf)) {yesno= true;  return; } else {yesno= false; }
  
} 

///////////// check ESP client connect to server status, 3 = connected ///////
void ESPstatus()  {
   /////// get message ////////
    char colon[] = ":";    // STATUS:3
     char msg[5]; msg[0] = '1'; msg[1]='\0' ; 
     
     Serial.println(F("AT+CIPSTATUS"));
    Serial.setTimeout(5000); 
    if (Serial.find(colon)) {Serial.readBytes(msg, 1);  msg[1] = '\0';} else {yesno = false; c_svr = false; }
    switch (msg[0]) { 
      case '3': c_ap = true;  c_svr = true;  yesno = true;  break; //status: 3 connected 
      case '1': c_ap = false;  break; // no msg, =1
      case '2': c_ap = true;   break; // status: 2 got IP
      case '5': c_ap = false;  break; // status: 5 not connect to AP
      case '4': c_ap = true;   break; // status: 4 connect to AP not connect to SVR
      } 
      //lcd.setCursor(15,0); 
      if (msg[0] != '3') {c_svr = false; yesno = false; //lcd.print(msg); delay(1000);
      } 
      
} 

/*
///////// Find WiFi AP SSID ////////// 
void findSTA()  {
    char msg[33]; msg[0]='\0' ; int last_pos = 0; 
    Serial.setTimeout(2000);
    Serial.find('"'); //  "Tayler_Home" 
        last_pos = Serial.readBytesUntil('"', msg, 32);  msg[last_pos] = '\0';
        if (last_pos ==0) {c_ap = false;} else {c_ap = true;}
        if (strcmp(msg, SSID)==0) {newssid = false;} else {newssid = true;} 
       
}   
*/
//////////// get Server reply message /////////
void SVRreply ()  {
  
    char datastart[] = "((";
    char msg[50]; msg[0]='\0' ;  bool extramsg;
    int last_pos;
   /////// get message ////////
   Serial.setTimeout(5000);
   if (!Serial.find(datastart)) {c_svr = false; return;}
   last_pos = Serial.readBytesUntil(')', msg, 50);  msg[last_pos] = '\0';
   if (last_pos == 0 || last_pos == 50) {c_svr = false; return;}
   c_svr = true; c_ap = true; MSG = String(msg); //lcd.setCursor(0,1); lcd.print(MSG);lcd.print("    "); 
   /////////// judge reply is yes or no///////// 
   if (MSG =="no" || MSG == "invalid") {yesno= false; } else {yesno = true;} 
   
   /////// get extra message ////////////
   if (MSG == "invalid") {       // there will have error message
       if (!Serial.find(datastart)) extramsg = false; else extramsg = true; 
       if (extramsg) {last_pos = Serial.readBytesUntil(')', msg, 32);  msg[last_pos] = '\0';}
       if (last_pos == 0 || last_pos == 32) extramsg = false; extramsg = true;
       if (extramsg) {MSG = String(msg); lcd.setCursor(0,1); lcd.print(MSG); lcdspace2();delay(5000);}
       } // if MSG
}

//////////// get Server reply message and Date: /////////
void SVRtime()  {
    char date[] = "Date: "; char space[] = " "; char colon[] = ":"; 
    char msg[30]; msg[0]='\0'; //char webwday[4]; 
    int last_pos; int webhour, webmin, websec; //webday, webweekday; 
    static time_t old_t; long t_kh_t, t_old_t;
    Serial.setTimeout(5000);
   //////// get server time /////////Date: Sat, 01 Apr 2017 11:34:53 GMT
    if (!Serial.find(date)) {c_svr = false; //lcd.setCursor(15,0); lcd.print("S"); 
                     return;}  c_svr = true; c_ap = true;
    
    Serial.readBytes(msg, 3);  msg[3] = '\0';  //Date: Sat,   get webweekday//////
      //switch (msg[1]) { case 'u': if (msg[0] == 'S') webweekday=0; else webweekday =2; break; case 'o': webweekday=1; break; case 'e': webweekday=3;break; case 'h': webweekday=4; break; case 'r': webweekday=5; break; case 'a': webweekday=6; break;} 
    Serial.find(space); Serial.find(space); Serial.find(space);   //Date: Sat, 01 Apr 2017 
    Serial.find(space); Serial.readBytes(msg, 2);  msg[2] = '\0'; webhour = atoi(msg);    ////// get userhour
    Serial.find(colon); Serial.readBytes(msg, 2);  msg[2] = '\0'; webmin = atoi(msg);   ////// get webmin
    Serial.find(colon); Serial.readBytes(msg, 2);  msg[2] = '\0'; websec = atoi(msg);   ////// get web second
   
   /////////// calculate user time and set arduino time     
   //webday = webweekday +1; 
   //time_t kh_t = now();  // remember current kh system time for time adjustment
   setTime(webhour, webmin, websec, 1, 1, 2017);  // 2017/1/1, sunday -> weekday =0
   time_t t = now(); t += timezone*3600; setTime(t);   // set time with time zone
   
} 

/*
void lcdprintesp ()  {
   // char buf[6] = {'\0'}; strcpy(buf, ipd);
    char msg[600]; msg[0]='\0' ; int last_pos = 0; byte button =0;
     unsigned long Countstart= millis(); long timeout = 3000;
   
   ////// another way to get message //////
      while (millis()-Countstart < timeout) {
          if (Serial.available()) {
                  char c = Serial.read();
                  msg[last_pos]=c ;last_pos++;
                  Countstart = millis();  // if serial not available > timeout then end
          } // if
          if (last_pos > 599) {break;}
   }  // while
   msg[last_pos] = '\0';    // ending with \0    
   
   //////// print or process message ///////
   lcd.clear(); lcd.setCursor(0, 0);  lcd.print("msg size "); lcd.print(last_pos); delay (1000);
   for (int i = 0; i < last_pos;) {
      for (int R = 0; R < 2; R++) {
          lcd.setCursor(0, R); lcd.print(F("                ")); 
          for (int C = 0; C < 16; C++) {
           lcd.setCursor(C, R); 
                button = ReadButton();
                if (button >0) {i=last_pos;}
                if (i < last_pos) {lcd.print(msg[i++]) ; delay(200);}
           else {lcd.print(' ') ;}
           } //for C
      }  //for R
   } // for i
}
*/

////////////// ESP 8266 wifi data transfer setup
////// Set esp8266 as AP and Server
bool APServer(int SID) {             // mode2 to enable AP
     char ok[] = "OK";      
     String sap; 
     if (SID==1) {
        lcd.clear(); lcd.print(F("WiFi Setup      ")); 
        sap=F("AT+CWSAP=\"Aquawiz_KH\",\"\",");sap += wifichannel; sap += F(",0");  
     } else {
        sap=F("AT+CWSAP=\""); sap+=SER; sap+=F("\",\"0922473267\",");sap += wifichannel; sap += F(",4");
     }
     //Serial.println(F("AT+CWSAP=\"Aquawiz_KH\",\"\",wifichannel,0")); ESPreply(ok, 10);         
     //Serial.println(F("AT+CWSAP=\"Aquawiz_KH\",\"1234567890\",9,4")); ESPreply(ok, 20); // set password 1234567890, last is encoding mode 1 WEP
     
     //ResetWiFi(); delay(2000);
     Serial.println(F("AT+CWMODE=2")); ESPreply(ok, 2); delay(1000);  // set AP mode
     Serial.println(sap);ESPreply(ok, 20);    // set SSID and pass
     Serial.println(F("AT+CIPMUX=1"));ESPreply(ok, 2);  // 
     Serial.println(F("AT+CIPSERVER=1,80")); ESPreply(ok, 2); 
     if (yesno) {return true;} else {return false;}
}        

////////// wifi setup to connect to AP /////////
void connectWiFi() {
  char ok[] = "OK"; bool wifi;
  
     for (int retry = 0; retry < 2; retry++){
       if (retry > 0) {lcd.setCursor(0, 1);lcd.print(F("    Retry "));lcd.print(retry);lcdspace4();}
       ResetWiFi(); delay(2000);
       Serial.println(F("AT+CIPMUX=0"));ESPreply(ok, 2);
       Serial.println(F("AT+CWMODE=1")); ESPreply(ok, 2);
       if (yesno) {wifi = true;} else {wifi = false;}   // define wifi fail or not
       
       String cmd =F("AT+CWJAP=\""); cmd+=SSID; cmd+="\",\""; cmd+=PASS; cmd+="\"";
       Serial.println(cmd); 
       ESPreply(ok, 20); if (yesno) {c_ap = true; break;} else c_ap = false; // define AP fail or not
       if (!wifi)  {lcd.setCursor(0, 1);lcd.print(F("   WiFi Fail !  ")); delay(2000); }
       if (wifi && !c_ap) {lcd.setCursor(0, 1);lcd.print(F("    AP Fail !   ")); delay(1000);}  
     }  // for
    
}
    
void ResetWiFi() {
  char READY[] = "ready"; char ok[] = "OK";
  Serial.println(F("AT+RST"));ESPreply(ok, 2); 
}

//////////////////// Sync Web site //////////////
void SyncWeb() {
   byte i,p; byte tryi =2; char ok[] = "OK"; char msg[5];//char ipd[] = "IPD,";
   int Fieldvalue; //Halt = false;  // reset Halt to check timer
   String SERIES=String(SER),host;
   if (localhost==1) {
       url[1] = F("Host: 192.168.0.27\r\n");
       host=F("Host: 192.168.0.27\r\n\r\n");
       } else {
       url[1] = F("Host: server.aquawiz.net\r\n");
       host=F("Host: server.aquawiz.net\r\n\r\n");} 
   
   int hr1old,hr2old;unsigned long Field25=0;
 /////////////////// GET POST /////////////////////////////
    connectWiFi(); //connect AP
        
    lcd.clear(); lcd.setCursor(0,0); lcd.print(F("Sync    ")); 
    lcd.setCursor(0,1); lcd.print(F("Web Data & Input"));  
    
    // Step 0 ////// get time /////////
       lcd.setCursor(4,0); lcddot();
       url[0]= F("GET /currentdate"); url[0] += F(" HTTP/1.1\r\n");   
       url[0] +=host; 
       
       for (i=0; i< tryi; i++) {UpdateIOT1(1, true); SVRtime(); if  (!c_ap || c_svr) {break;} }
       if (setSER == 1) {Serial.println(F("AT+CIPCLOSE"));ESPreply(ok, 2);
                         Serial.println(F("AT+CWQAP"));  return;}    // if series not set, then get time only, no run pending config
                         
    //Step 1 /////// check Pending config ///return ((valid)) ((invalid))////// 
       lcd.setCursor(5,0); lcddot();; 
       url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/pending-config/field3"); url[0] += F(" HTTP/1.1\r\n");
       url[0] +=host;   
             
       for (i=0; i< tryi; i++) {UpdateIOT1(1,true); SVRreply(); if (!c_ap || c_svr) {break;}}
       //if (!yesno || !c_svr) { return;}         // if not yes, return, if yes, start parameter exchange 
       if (yesno) {         // if yes, then start config procedure
       
   //Step 2///// Start Check password, return ((valid)), ((invalid)) ///////
       lcd.setCursor(6,0); lcddot();
       url[0] = F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/start/field4"); url[0] +=F(" HTTP/1.1\r\n");
       url[3]= F("{\"field4\":\""); url[3] += WebKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\n\r\n"); 
       url[0] = url[0]+url[1]+url[2]+url[3];
             
        for (i=0; i< tryi; i++) {UpdateIOT1(1); SVRreply(); if (!c_ap || c_svr) {break;}}
        if (!yesno || !c_svr) {return;}
       
   
   //Step 3////// ask field 8 Set Tank KH, ////return ((7500))//////     
       lcd.setCursor(7,0); lcddot();  
       url[0] =F("GET /api/v1/KH/");url[0] += SERIES; url[0] += F("/field8"); url[0] += F(" HTTP/1.1\r\n");
       url[0] +=host; 
             
       for (i=0; i<tryi; i++) {UpdateIOT1(1,true); SVRreply(); if  (!c_ap || c_svr) {break;}}
       if (!yesno || !c_svr) {return;} 
              
       for (i=0; i<48; i+=2) {
         MSG.substring(i,i+2).toCharArray(msg, 3);   // String "0123456", "01" = substring(0,2), "23" = substring(2,4)
         p=i/2;                                      // msg {0,1,/0}= toCharArray(msg,3)
         CO2pulse[p]= strtol(msg, NULL, 16);
       }
       memcpy (CO2new, CO2pulse, sizeof(CO2new)); // copy CO2pulse array to CO2new array
       
                     
   //Step 4////// ask field 12 Mode, return ((12))  //////////
           //Cmode (1 char) + TuneNow (1) + TunePast(1) + CO2period (2) + SetCaPH (2)   
       lcd.setCursor(8,0); lcddot();
       url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field12"); url[0] += F(" HTTP/1.1\r\n"); 
       url[0] +=host; 
             
       for (i=0; i<tryi; i++) {1, UpdateIOT1(1,true); SVRreply(); if  (!c_ap || c_svr) {break;}}
        if (!yesno || !c_svr) {return;} 
        
        Fieldvalue = MSG.substring(0,1).toInt(); if (Fieldvalue >=0) { Cmode = Fieldvalue; }
        Fieldvalue = MSG.substring(1,2).toInt(); if (Fieldvalue >=0) { TuneNow = Fieldvalue; }
        Fieldvalue = MSG.substring(2,3).toInt(); if (Fieldvalue >=0) { TunePast = Fieldvalue; }
        Fieldvalue = MSG.substring(3,5).toInt(); if (Fieldvalue >= 2) { CO2period = Fieldvalue; }
        Fieldvalue = MSG.substring(5,7).toInt() *100; if (Fieldvalue >=0) { SetCaPH = Fieldvalue; }
        
    //Step 4.1////// ask field 5 ValveOpen angle //////////
    // OpenAng (2 char) + KHdosing (3) + Vue.maxKHdosingPerHr (3 );   
       lcd.setCursor(9,0); lcddot();;
       url[0] =F("GET /api/v1/KH/");url[0] += SERIES; url[0] += F("/field5"); url[0] += F(" HTTP/1.1\r\n");
       url[0] +=host; 
             
       for (i=0; i<tryi; i++) {UpdateIOT1(1,true); SVRreply(); if  (!c_ap || c_svr) {break;}}
       if (!yesno || !c_svr) {return;} 
       
       Fieldvalue = MSG.substring(0,2).toInt(); if (Fieldvalue >=0) { if (!NewOpenAngFlag) {OpenAng = Fieldvalue;}}
       Fieldvalue = MSG.substring(2,5).toInt(); if (Fieldvalue >= 0) { mldKH = Fieldvalue;}
       Fieldvalue = MSG.substring(5,8).toInt(); if (Fieldvalue >= 0) { maxml = Fieldvalue; }
       if (maxml ==0) {TuneNow=0;TunePast=0;}    
           
   //Step 6///// end of transaction, return ((yes)) ////////// 
      lcd.setCursor(12,0); lcddot();
      url[0] = F("GET /api/v1/KH/");url[0] += SERIES;  url[0] += F("/eot"); url[0] += F(" HTTP/1.1\r\n"); 
      url[0] +=host; 
      
      for (i=0; i< tryi; i++) {UpdateIOT1(1,true); SVRreply(); if (!c_ap || c_svr) {break;}}
      if (!c_svr) {return;}
       
        
    //Step 7///// Ask new password, return new password ((k328)) or ((invalid))//////////
      //lcd.setCursor(4,0); lcd.print(F("        ")); 
      lcd.setCursor(13,0); lcddot();
       url[0] =F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field2 HTTP/1.1\r\n"); 
       url[3]= F("{\"field2\":\""); url[3] += WebKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\n\r\n"); 
       url[0] = url[0]+url[1]+url[2]+url[3]; 
       
       char NewKey[6];      
        for (i=0; i<tryi; i++) {UpdateIOT1(1,true);  SVRreply(); if  (!c_ap || c_svr) {break;}}  //    return "3gS5" or "invalid"  without "yes" or "valid"
        if (!yesno || !c_svr) {return;}     
          
        MSG.toCharArray(NewKey, 6); 
                
   //Step 8///// Confirm new password, return ((valid)), ((invalid)) //////////
       lcd.setCursor(14,0); lcddot();
       url[0] =F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/field4 HTTP/1.1\r\n"); 
       url[3]= F("{\"field4\":\""); url[3] += NewKey; url[3] += F("\"}\r\n");
       url[2] = F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\n\r\n"); 
       url[0] = url[0]+url[1]+url[2]+url[3]; 
       
      for (i=0; i< tryi; i++) {UpdateIOT1(1,true); SVRreply(); if  (!c_ap || c_svr) {break;} }
       if (!c_svr) {return;} 
       strcpy(WebKey, NewKey); EPROMsave();
 
   }   // if (yesno) pending config

   
   // Step 9//// upload data ///////////////////////////////////////////////////////
    if (newdKH || NewOpenAngFlag) {
       
      lcd.setCursor(15,0); lcddot();    
       hr1old = hour()-1; if (hr1old < 0) {hr1old=23;} // 一小時前
       hr2old = hr1old -1; if (hr2old < 0) {hr2old=23;} // 兩小時前
       Field25 = (long)hr2old * 100000 + (long)OpenAng * 1000 + (long)CO2pulse[hr2old]; // 合併 time index  hr2old and CO2pulse[hr2old]
       NewOpenAngFlag = false; 
       ////// update to aquaserver //////
       
       
       url[0] = F("POST /api/v1/KH/");url[0] += SERIES;  url[0] += F("/meas"); url[0] +=F(" HTTP/1.1\r\n");
       url[3]= F("{\"field4\":\""); url[3] += WebKey; 
       url[3] += F("\", \"field22\":"); url[3] += dKH[hour()]*10;  // KH difference to target KH value
       url[3] += F(", \"field23\":"); url[3] += CO2pulse[hour()];  // CO2 bubble setting    
       url[3] += F(", \"field24\":"); url[3] += PHminSync;           // CaRx PH min value after finish CO2 bubble
       url[3] += F(", \"field25\":"); url[3] += Field25;  //CO2new[2]; // CO2 bubblw modified for past two hours. (hour + OpenAng + CO2pulse)
       url[3] += F(", \"field26\":"); url[3] += CO2new[hour()];      //CO2 bubble actual after fixed
       url[3] += F("}\r\n");
       url[2]=  F("Content-Length: "); url[2] += url[3].length(); url[2] += F("\r\n\r\n"); 
       //url[0] = url[0]+url[1]+url[2]+url[3];
       
       for (i=0; i< tryi; i++) {UpdateIOT1(1,false); SVRreply(); if (!c_ap || c_svr) {break;}}
       
    }  // if newdKH
    Serial.println(F("AT+CIPCLOSE"));ESPreply(ok, 2); CRAserver();// quit connect  and become CRA AP
}


///// upload data /////////
void UpdateIOT1(int IP, bool url0) {
  char prompt[] = ">"; char ok[] = "OK"; 
      byte i =0; byte ms=20;      
    
    ///////// connect Server /////////  
    ESPstatus();
    if (!c_ap) {connectWiFi();}
    if (!c_ap) {return;}  // if fail to connect AP then return 
    if (!c_svr) {
        for (i=0; i< 3; i++ ) {  // try 3 times to connect server
          if (localhost==1) {Serial.println(F("AT+CIPSTART=\"TCP\",\"192.168.0.27\",8888")); }
          else {Serial.println(F("AT+CIPSTART=\"TCP\",\"server.aquawiz.net\",80")); } 
          cleanbuffer();     // delay sometime to wait server reply 
          ESPstatus();  if (yesno) {break;}
        } // for
    }  // if !c_svr
    if (!c_svr) {lcd.setCursor(0, 1);lcd.print(F("   WWW Fail !   ")); delay(2000); return;}  // if fail to connect server then return
   
             
       //////// CIPSEND command send url length   //////////  
       if (url0) {
       delay(ms);Serial.print(F("AT+CIPSEND=")); Serial.println(url[0].length());   
       ESPreply(prompt, 5); if (yesno) {Serial.print(url[0]);}
       } else {  // if url0
       for (i=0; i<4; i++) { delay(ms);    /// url[0]-url[3]
               Serial.print(F("AT+CIPSEND=")); Serial.println(url[i].length());   
               ESPreply(prompt, 5); if (yesno) {Serial.print(url[i]);}
               if (i < 3) {ESPreply(ok, 5);} 
               if (!yesno) {break;}
           }  // for i
       }  // else url0
         
}     
  

/////////// Control KH ////////////
void ControlPH() { 
   //static unsigned long idle_start;
   unsigned long idle, remaintime,remainBubble;
   bool Halt2;
   int KHoutTime=45, Flowtime, Bubbletime;
   lcd.clear(); ReadPHvalue(); DisplayPHvalue(); 

  if (!Halt) { Halt2 = false;}          //  if Halt = false, no halt, // another Halt that can penetrate while {
  else { Halt = false; Halt2 = true;}   //if Halt = true, halt before, then reset Halt and keep StepId, but set another Halt2 to penetrate while
         
  while (!Halt) { 
     if (testmode!=2) {if (now() < 7200) {SyncWeb();}} // just power up, now=0
     if (!Halt2) {thr = hour();} else { Halt2 = false;} // if not Halt, then reset thr = current hour()
    for (int z = StepId; z < 4; z++) {
    switch (z) {
    
    case 1:   // dosing KH 
        Step =1;
        //////// CO2 output to push back upstream water from CaRx to CRA /////
         digitalWrite(CO2valve, HIGH); digitalWrite(CO2valve1, HIGH);delay (50);  
         digitalWrite(CO2valve, LOW);digitalWrite(CO2valve1, LOW); 
         
        if (CO2pulse[thr] > 120 && Cmode==0) {Cmode = 1;} // change Cmode to 1 as bubble high mode 
    
        if (Cmode == 2) {Flowtime = CO2new[thr];; // CO2pulse in Cmode=2 PH control mode is flow time
                         if (Flowtime > 60) Flowtime=60; idle = (60-Flowtime)*60;  // limit to flow < 60 minutes  
                         Valve_on();Count_S(Flowtime*60, false, 2);Valve_off();} // PH control mode
        else if (Cmode == 1) {CO2period=3600/(CO2new[thr]+1); idle =0;  // CO2pulse in Cmode=1 is bubble count, and convert to period 
                         Valve_on();Count_S(3600, false, 1);} // continue 1 hour flow mode for bubble high control mode
        else            {Bubbletime = CO2period*CO2new[thr]; // Cmode = 0  Bubble low mode
                         if (Bubbletime > 1800) {Bubbletime = 1800; CO2period=1800/(CO2new[thr]+1);} // limit bubble time < 30 min to keep flow out time 30 min
                         idle = 3600 - Bubbletime;
                         Valve_off(); Count_S(Bubbletime, false, 1); Valve_on(); }  // Bubble low control mode
        if (StartNewBubble==1) {z=2;}  //if timer to xx:45, then start from step 1 z=0+1 for a new bubble procedure
        break;
    
    case 2:   // idle time for Cmode 0, 2 to complete 1 hour period
        Step=2;
        if (Cmode ==2) {Valve_off(); Count_S(idle, false, 2);Valve_on();}  // PH control mode, no flow
        if (Cmode ==1) {Valve_on(); Count_S(idle, false, 1);}  // bubble high control mode
        else {Valve_on();Count_S(idle, false, 0); Valve_off();}
                     // Bubble mode (idle time, ph gatting, print 2. flow time 1. bubble count. 0. no print) 
        //if (StartNewBubble==1) {z=2;}  //if timer to xx:45, then upload data z=3 and then start from step 1 z=1 for a new bubble procedure
        break;
    
    case 3:
         Step=3;
         StartNewBubble=2; // to prevent run step 1 again if no bubble
         
         //////// flush procedure ///////// 23:xx 開始執行Flsuh 程序
         if (FlushFlow==false && thr==flush_hr) {
             FlushFlow = true; FlowServo (waterservo, 95, 2000); // full open valve  
             lcd.setCursor(5,1);lcd.print(F("Flush ")); 
             for (int f=0; f<60; f++){ lcd.setCursor(11,1);lcd.print(60-f); lcdspace3();delay(1000);}
             FlowServo (waterservo, OpenAng, 2000); } // back to flow setting    
         if (FlushFlow==true && thr==flush_hr+1) {    // 離開 23:xx　reset FlsuhFlow 重新定義 flush hour
             FlushFlow = false; 
             int CO2min = CO2pulse[flush_hr]; 
             for (int f=0; f<24; f++){ if (CO2pulse[f] < CO2min) {CO2min = CO2pulse[f]; flush_hr = f;}}  
         } // if FlushFlow
         
         //////// force to get KH if not before /////
         if (!newdKH) {GetKH();} //沒收到KH controller信號，強迫去GetKH()設定 CO2new
         break;
        
    }  // switch
        if (Halt) {StepId = z;break;}  // record stepID for break
         }  // For loop
       if (Halt) {break;}    //  break while 
       StepId = 1;        // reset StepId =1 to start from 1st step, should be put after break
    }  // while Halt
    
}    


void Valve_on() {
     //lcd.setCursor(7,0); lcd.print(F("1")); 
     //int pos_on= 95;              // delay 2 sec to make sure valve is on   
     FlowServo (waterservo, OpenAng, 2000); 
     lcd.setCursor(11,0); lcd.print(F("     ")); 
}

void Valve_off() {    
    //lcd.setCursor(7,0); lcd.print(F("0")); 
     int pos_off = 0;           // delay 2 sec to make sure valve is off   
     FlowServo (waterservo, pos_off, 2000);
     lcd.setCursor(11,0); lcd.print(F("     ")); 
}

void FlowServo (int servo, int openi, int t) {
  unsigned long C_start, Elapse =0; int pwm, angle;
  if (openi <0) {openi=0;}
  angle=95-openi;    // to transform open to angle, angle0=open95, angle95=open0
 while (Elapse < t) {
  C_start = millis();
  pwm = (angle*11) + 500;      // Convert angle to microseconds
  digitalWrite(servo, HIGH);
  delayMicroseconds(pwm);
  digitalWrite(servo, LOW);
  delay(15);   // Refresh cycle of servo
  Elapse += (unsigned long)(millis() - C_start); 
} // while
}  

   
void Count_S(long t, bool ph, int ft) {
  char dev[] = "DEV=";
  unsigned long C_start; static unsigned long Elapse; 
  int Elapse_s= Elapse/1000; ; int CO2count=0, minsec; //char buf[10];
  bool LCDlight=true; int LCDtime=180;
  int start_sec=2700; // xx:45min = 2700 sec
  Serial.setTimeout(10);  // set serial time out 100ms to reduce lag in while loop
  
    if (t <= 0) {t = 2;} // for logic to check minute = 45
  while (Elapse_s < t) {    // count time
      C_start = millis();
      minsec = minute()*60 + second(); // current time
      ReadPHvalue(); DisplayPHvalue(); //delay(500);

      //////// receive dKH procedure ///////
      //if (Serial.available()) {lcdprintesp();}
      if (Serial.find(dev)) {lcd.backlight(); LCDlight=true; LCDtime = Elapse_s + 180;GetKH();} //收到KH controller信號，去GetKH()
      
      /////// Start new bubble procedure /////
      if (minsec >= start_sec && minsec < start_sec+30) {
          StartNewBubble ++; delay(500);  // delay 0.5 sec 避免累積太快
          if (StartNewBubble == 1) {PHminSync = PHmin; PHmin=10000; break;}} //StartNewBubble=1 才跳出 
                    // xx:45分 跳出以開始執行新的 bubble 程序 StartNewBubble++ 可防止在30秒內多次執行 new bubble
      
      //////// sync procedure ////////
      if (minsec >= Sync_sec && minsec < Sync_sec+60 && Syncyet==false) 
              { if (Sync_sec < start_sec) {PHminSync = PHmin;}  // if web sync before xx:45, then current PHmin upload
                lcd.backlight(); LCDlight=true; LCDtime = Elapse_s + 180;
                SyncWeb(); Syncyet=true; lcd.clear();}  
              // update data, password and get parameter and reset PHmin} // to sync time 跳出以開始執行 sync 程序
      //////// flag reset at hour end ///////
      if (minsec >=3590) {Syncyet = false; Sync_sec=2700; newdKH=false; StartNewBubble=0;  } 
                          // reset sync flag,Sync_sec, newdKH flag  at xx:59 after newbubble and sync complete
      
      if (ph && PHvalue < SetCaPH) {break;}
      if (ft==2) {lcd.setCursor(5,1); lcdprintDigits3(Elapse_s/60);} 
      if (ft==1) {lcd.setCursor(5,1); lcdprintDigits3(CO2count);}
      
      if (ft!=0 && t>5 && Elapse_s%CO2period ==0 && PHvalue > SetCaPH) {  //if time = period of CO2 on/off
        ////////// CO2 valve on & Off to release CO2 one time
        digitalWrite(CO2valve, HIGH);digitalWrite(CO2valve1, HIGH); delay (1000);      // CO2 valve on
        digitalWrite(CO2valve, LOW); digitalWrite(CO2valve1, LOW); 
        CO2count+=1; // count CO2
      }  // if CO2period and PH value > SetCaPH
      
      if (Elapse_s > LCDtime) {lcd.noBacklight(); LCDlight=false; } //no backlight
      if (ReadButton() != 0) {
          if (!LCDlight) {lcd.backlight(); LCDlight=true; LCDtime = Elapse_s + 180;}
          else {Halt = true; break;} //  break while loop 
      }  //if
      Elapse += (unsigned long)(millis() - C_start); Elapse_s = Elapse/1000;  // Count in second  
  }  // while 
      Elapse = 0;  
     lcd.backlight();
}

///////////////////////////////////////////////////
//  Wifi data transfer : CRA server == KHA client 
///////////////////////////////////////////////////
void GetKH() {
    unsigned long C_start, waitime, waitime1, waitime2; bool valovr=false; 
    int  CO2,hr1old,hr2old, fixml, kh_dis, maxml_1; 
        
    ///////// wait for message come in /////////////////
    char dev[] = "DEV=", khd[] = "dKH=", kha[] = "KHA=", ipd[] = "IPD,", pair[20], msg[20]; msg[0]='\0';; int dkh, TankKH,last_pos =0;
    String PAIR, PAIRED=String(DEV);
    /////// get message from KHA ////////
    last_pos = Serial.readBytesUntil('&', msg, 19); msg[last_pos] ='\0'; 
    if (last_pos == 0 || last_pos == 19) {PAIR=PAIRED; dkh=0;} // if receive nothing, then it is forced 
    else {
      urldecode(pair, msg); PAIR=String(pair);
      Serial.find(khd); dkh = Serial.parseInt();  
      Serial.find(kha); TankKH = Serial.parseInt(); 
      lcd.setCursor(0,0);lcd.print(F("KHA "));
    }
    if (PAIR != PAIRED) {lcd.setCursor(0,1); lcd.print("Not Paired Dev  "); dkh=0;}
    //else{lcd.setCursor(0,1); lcd.print("dKH=");lcd.print(dkh); lcd.print("KHA=");lcd.print(TankKH);}
    delay(2000);
    
    dKH[hour()] = -1*dkh/10;  // same format as KH out signal
    newdKH = true; // set newdKH flag
    /////////////////////////////
    hr1old = hour()-1; if (hr1old < 0) {hr1old=23;} // 一小時前
    hr2old = hr1old -1; if (hr2old < 0) {hr2old=23;} // 兩小時前

    ///////// KHA 輸出修正 KH 差異 > 3 dKH, KHA 輸出錯誤 從 -1.0(dKH-100)變 1.0(dKH100) /////舊版KHA問題
    //kh_dis = abs(dKH[hour()]-dKH[hr1old]);
    //if (kh_dis > 190) {dKH[hour()]=dKH[hr1old];} // 如果輸出距離dKH 大於 (190) 200，則為輸出錯誤，令現值=前值 
    /////////////////////////////////////////////
    
    CO2new[hour()] = CO2pulse[hour()]; //先設定跟default相同
    if (TuneNow==1) {    // 自動調整 每小時設定, 修正現在值 以追上設定KH值   
             fixml=mldKH*dKH[hour()]/100;
             if (abs(dKH[hour()]) > 20){maxml_1 = maxml;} else {maxml_1 = maxml/10;} // if KH error dKH > 0.2 最大調整值= maxml
                 if (fixml>0 && fixml > maxml_1){fixml=maxml_1;}                 // if KH error dKH < 0.2 最大調整值= maxml/10   
                 if (fixml<0 && -1*fixml > maxml_1) {fixml=-1*maxml_1;} 
             
             CO2 = fixml + CO2pulse[hour()]  ; // 現在缺多少補多少，太多就減少
             if (CO2 > 240) {CO2 = 240;} if (CO2 < 0) {CO2 = 0;} 
             CO2new[hour()] = CO2; 
    }   // if TuneNow.  
    
    if (TunePast==1) {  // 修正舊值以使 KH不變，維持穩定KH
             fixml= mldKH*(dKH[hour()]-dKH[hr2old])/100; 
             if (fixml>0 && fixml > maxml){fixml=maxml;}
             if (fixml<0 && -1*fixml > maxml) {fixml=-1*maxml;}
             CO2 = fixml + CO2new[hr2old];   // 修正兩小時前放出氣泡數，使kh不變，穩定
             if (CO2 > 240) {CO2 = 240;} if (CO2 < 0) {CO2 = 0;} 
             CO2pulse[hr2old]=CO2; // apply new value to last two hour since last two hour not match the result  
        EPROMsave(); // save modified old CO2pulse setting 
    } // if TunePast  

    
    // sync time setting
    int series = String(SER).substring(7).toInt(); //; CA1-00-00065; become 00065 ; int series_int = series.toInt();
    int minsec = minute()*60 + second(); // current time
    int remsec = 3400 - minsec; // remain time can spread web sync
    Sync_sec = minsec + remsec/100*(series%100);  // current time(1800) + remain time(1600)/100 * serial%100    keep Period1 in step 12 > 200 sec 
    
                
}      


////////////////// Sub Read and smooth PH value by numReadings = 1000 times
void ReadPHvalue() {
   long num10_7=3010L,num7=7000L, ph7=(long)PH7, ph10=(long)PH10, ph10_7 = (long)abs(ph10-ph7); 
   int maxi=1000;
   total = 0;
    for (int i=0; i< maxi; i++) {total = total + (long)analogRead(A0);}
    total = (float)total/100.0 + 0.5;
    
    PHvalue = num7 + num10_7 * (total-ph7) / ph10_7; 
    if (PHvalue < 3000) {PHvalue=3000;} if (PHvalue > 10000) {PHvalue=10000;} // upper and lower limit for PH value
    if (PHvalue < PHmin){PHmin = PHvalue;} // find minimum PH value for low CO2 warning
} 

//////////////////// Sub Display PH value on lcd
void DisplayPHvalue(){
    static bool adjflag;
    lcd.setCursor(0, 0); 
       if (newdKH) {lcd.print((float)dKH[hour()]/-100.0,2);} // display current measured KH value
       else{lcd.print((float)dKH[thr]/-100.0,2);} lcdspace1(); 
    lcd.setCursor(6,0);  lcd.print(F("M"));lcd.print(Cmode); lcd.print(F("A")); lcd.print(Step);// resume display
     
    //if (now()%(36000/time_adj) <20) { if (adjflag==false) {adjustTime(sign(time_adj)); adjflag=true;}}   // add 1 sec after (3600/time_adj) second.
    //else {adjflag=false;} 
    lcd.setCursor(11, 0);lcdprintDigits(hour());lcd.print(":");lcdprintDigits(minute());//lcd.print(":");lcdprintDigits(second());
    
    int i; float PHfloat = (float)PHvalue/1000.0; if (PHfloat < 9.99) {i=2;} else {i=1;}
    lcd.setCursor(0, 1); lcd.print(PHfloat, i);lcdspace1();  // print current PH 
    lcd.setCursor(8, 1); lcd.print(F("/"));lcdprintDigits3(CO2new[thr]);lcd.print(F("/"));// print new CO2 pulse count
    lcd.setCursor(13, 1); lcdprintDigits3(CO2pulse[thr]); // print original CO2pulse, CO2 pulse count
    delay(200); dac.setVoltage(dacKH(PHvalue), false); // PH output through DAC
  }  // void

 /////////////// set clock date and time ////////////////////

int sign(int x) { return (x>0) - (x<0); }

///////// lcd print time 1 digit ///////
 void lcdprintDigits(int digits){
  if(digits < 10) lcd.print("0");
  lcd.print(digits);
}
///////// lcd print 3 digit ///////
 void lcdprintDigits3(int digits){
  if (digits < 100) lcd.print("0");
  if (digits < 10) lcd.print("0");
  lcd.print(digits);
}

  
void lcdPH() {
    lcd.setCursor(11,1); lcd.print(total);  lcd.print("    "); // Refresh total read value
    lcd.setCursor(5,1);  if (PHvalue < 10000) {lcd.print((float)PHvalue/1000.0, 2);} 
                         else {lcd.print((float)PHvalue/1000.0, 1);}  // continue refresh PH value 
}


////////////////////// calibrate PH probe ///////////
  void CalibratePH() {
         //Adj (val, maxi, mini, stepi, row, int item)
     ////////////// PH output calibration  ////    
     int p=2; //x=2;
     lcd.clear(); lcd.print(F("PH Output Calib"));
     lcd.setCursor(0,1);lcd.print(F("KH -> PH = 7.0  "));
     
     p = Adj(p,3, 2, 1, 0, 4);
     dac.setVoltage(dacKH(7000), false);
     delay(1000);   
  
     //////////// ask calibrate or not ///////////
     p=0;
     lcd.clear(); lcd.print(F("PH  Input Calib "));
     p = Adj(p, 1, 0, 1, 0, 5); 
     //////////// Calibrate PH 7.00
     if (p==1) {
      lcd.clear(); lcd.print(F("PH  Input  Calib")); 
      lcd.setCursor(0, 1); lcd.print(F("PH7")); 
      p = Adj(p,3, 2, 1, 0, 2);PH7=total;
     ////////////Calibrate PH 10.0
      lcd.setCursor(0, 1); lcd.print(F("PH10")); 
      p = Adj(p,3, 2, 1, 0, 2);PH10=total;}

     /////////// set KHA serial ////////
     lcd.clear(); lcd.print(F("Pair KHA Serial ")); 
     p=0; p = Adj(p, 1, 0, 1, 0, 5); 
     if (p==1) {AdjDEV();} // input DEV

  EPROMsave();   // Write new PH7 and PH10 to EEPROM
  
  
 }   //void
 
 int dacKH (int kh) {
  if (kh<0) {kh=7000;}
  int dac = 4565.0-(float)kh/3.04;   //dac = kh*4096/14000 = tankkh/3.418
  if (dac <0){dac =0;} if (dac > 4095){dac=4095;}
  return dac;    
}

//////////////// Input Tank KH value
void InputCO2() {
     int start, count=0, x, OpenAng_old;
  
     ///////// set Flow rate by open angle   ////
     lcd.clear(); lcd.setCursor(0, 0); lcd.print("CaRx Flow Rate  ");
     lcd.setCursor(0, 1);lcdprintDigits(OpenAng); delay(1000);
     FlowServo (waterservo, OpenAng, 500);
     OpenAng_old = OpenAng;
     OpenAng = Adj (OpenAng, 95, 0, 1, 0, 1);
     if (OpenAng != OpenAng_old) {NewOpenAngFlag = true;}
     EPROMsave();
     
     ///////// CO2 output test   ////
     lcd.clear(); lcd.setCursor(0, 0); lcd.print("CO2 output test ");
        start=second();
        x = ReadButton();    // ReadButton
     while (x != 5) {         //  if select button not pressed then loop 
        if ((second()-start)%30 ==0) {count++; lcd.setCursor(0, 1);lcd.print(count);
        digitalWrite(CO2valve, HIGH);digitalWrite(CO2valve1, HIGH); delay (1000);      // CO2 valve on
        digitalWrite(CO2valve, LOW); digitalWrite(CO2valve1, LOW); } 
        x = ReadButton();
     } // while
     delay(1000);
 }   //void


void fpulse(int st) {
    int hlcd, x, h;
    for (x=0; x<=4; x++) {
       hlcd= x*3; h = st+x;
       if (h<=23) {lcd.setCursor(hlcd,1);lcdprintDigits(CO2pulse[h]);}
    } //for   
}
/*
void Reset() {
    lcd.clear(); lcd.setCursor(0, 0); lcd.print("Reset");
    setSER=1;
    PH7 = 5021;
    PH10 = 8720;
    OpenAng = 95;
    SetCaPH = 5000;
    CO2period = 30;
    Cmode = 0;
    TuneNow = 1;
    TunePast = 1;
    mldKH=200;
    maxml=20;
    EPROMsave();  // save to EEPROM
    delay(1000);lcd.setCursor(6, 0); lcd.print("OK"); delay(1000);
}
*/
void EPROMsave() { 
      CAparameter P;
       P.setSER = setSER;
       P.PH7 = PH7;
       P.PH10 = PH10;
       P.OpenAng = OpenAng;
       P.SetCaPH = SetCaPH;
       P.CO2period = CO2period;
       P.Cmode = Cmode;
       P.TuneNow = TuneNow;
       P.TunePast = TunePast;
       P.mldKH = mldKH;
       P.maxml = maxml;
       memcpy (P.CO2pulse, CO2pulse, sizeof(P.CO2pulse));
       P.timezone = timezone;
       P.wifichannel = wifichannel;
       P.lcdadr = lcdadr;
       strcpy (P.WebKey, WebKey);
       strcpy (P.SSID, SSID);
       strcpy (P.PASS, PASS);
       strcpy (P.SER, SER);
       strcpy (P.DEV, DEV);
       
       EEPROM.put( eeAddress, P);
}

////////////// initial setting SERIES number ////////
void AdjSER() {
   int p=0; String S=F("CA1-00-00");
   lcd.clear();
   lcd.setCursor(2,1);lcd.print(SER);
   delay(500);p= Adj(p,9, 0, 1, 11, 0);S += p; 
   delay(500);p= Adj(p,9, 0, 1, 12, 0);S += p;
   delay(500);p= Adj(p,9, 0, 1, 13, 0);S += p;
   S.toCharArray(SER, 13);
   lcd.setCursor(2,1);lcd.print(SER);delay(2000);
   setSER = 0; 
   EPROMsave(); // save SERIES and reset setSER
}
void AdjDEV() {
   int p=0; String S=F("KH1-00-00"); 
   lcd.setCursor(0,1);lcdspace2();lcd.print(DEV);
   delay(500);p=DEV[9]-'0';p= Adj(p,9, 0, 1, 11, 0);S += p; 
   delay(500);p=DEV[10]-'0';p= Adj(p,9, 0, 1, 12, 0);S += p; 
   delay(500);p=DEV[11]-'0';p= Adj(p,9, 0, 1, 13, 0);S += p; 
   S.toCharArray(DEV, 13);
   lcd.setCursor(2,1);lcd.print(DEV);delay(2000);
   
   EPROMsave(); // save SERIES and reset setSER
}
////////////// initial setting SERIES number ////////
int Adj (int val, int maxi, int mini, int stepi, int row, int item) {
     int valold,Count, r;
     int x = ReadButton();    // ReadButton
     while (x != 5) {         //  if select button not pressed then loop 
         x = ReadButton();
     if (x==3){val = val+stepi;}      // if up buttom pressed then add 1 digit
     if (x==2){val = val-stepi;}      // if down button pressed then minus 1 digit
     if (val > maxi) val = mini; if (val < mini) val = maxi; 
     //////// execution item //////////////
     if (item==4) {if (x==3) {val=3; dac.setVoltage(dacKH(10000), false);delay(500);} 
                       if (x==2) {val=2; dac.setVoltage(dacKH(7000), false);delay(500); } }        // switch PH7 & PH10
     if (item==1) { if (x==2 || x==3) { FlowServo (waterservo, val, 500); x=0;}}
     //////// blink item /////////////////
     Count= millis()/200; lcd.setCursor(row,1);
       if (Count%3 != 0) {         // flash on period  
         if (item==0) {lcd.print(val);}
         if (item==1) {lcdprintDigits(val);}// item =1, flow angle setup item 
         if (item==2) {ReadPHvalue(); lcdPH();}
         if (item==4) {lcd.setCursor(11,1); if (val==2) {lcd.print(F("7.0 "));} 
                                            if (val==3) {lcd.print(F("10.0"));}}
         if (item==5) {lcd.setCursor(0,1); lcd.print(F("No    Yes       "));}
                       
       }    // if Count      
     ///////// flash off period ///////////
       else {        // flash off period  
        if (item==0) {lcdspace1(); }
        if (item==1) {lcdspace2();}// item =1, flow angle setup item 
        if (item==4) {lcd.setCursor(11,1);lcdspace4();}  
        if (item==5) {if (val==0) {r=0;} else {r=6;} lcd.setCursor(r,1);lcdspace3(); }   
      }    // else Count%3 
         
     }   // while
     lcd.setCursor(row,1);
     if (item==0) {lcd.print(val);}
     if (item==1) {lcdprintDigits(val); FlowServo (waterservo, val, 1000);} // item =1, flow angle setup item
     if (item==2) {lcd.setCursor(11, 1); lcd.print(F("OK    "));}
     delay(500);return val;
} 
void lcdspace4() {lcd.print(F("    "));}
void lcdspace3() {lcd.print(F("   "));}
void lcdspace2() {lcd.print(F("  "));}
void lcdspace1() {lcd.print(F(" "));}
void lcddot() {lcd.print(F("."));}
